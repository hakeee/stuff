//
// Created by hakeee on 12/21/15.
//

#ifndef ALLOCATORS_SOURCE_INFO_H
#define ALLOCATORS_SOURCE_INFO_H

struct source_info {
    source_info(const char* file, int line, size_t size) :
            line(file) ,
            linenumber(line),
            size(size) {
    }

    const char* line;
    const int linenumber;
    const size_t size;
};

#endif //ALLOCATORS_SOURCE_INFO_H