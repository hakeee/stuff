//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_NO_BOUNDS_CHECKING_H
#define ALLOCATORS_NO_BOUNDS_CHECKING_H

class NoBoundsChecking
{
public:
    static const size_t SIZE_FRONT = 0;
    static const size_t SIZE_BACK = 0;

    inline void GuardFront(void*) const {}
    inline void GuardBack(void*) const {}

    inline void CheckFront(const void*) const {}
    inline void CheckBack(const void*) const {}
};

#endif //ALLOCATORS_NO_BOUNDS_CHECKING_H
