//
// Created by hakeee on 12/24/15.
//

#ifndef ALLOCATORS_STATIC_HEAP_AREA_H
#define ALLOCATORS_STATIC_HEAP_AREA_H

#include "../../../pch.h"

class HeapArea {
public:

    HeapArea(size_t size) :
            size(size)
    {
        buffer = new char[size];
    };

    ~HeapArea() {
        delete [] buffer;
    }

    void* GetStart() const {
        return (void*)&buffer[0];
    };
    void* GetEnd() const {
        return (void*)&buffer[size];
    };

    const size_t size;
    const char* buffer;
};

#endif //ALLOCATORS_STATIC_HEAP_AREA_H
