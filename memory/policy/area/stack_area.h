//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_STACK_AREA_H
#define ALLOCATORS_STACK_AREA_H

#include "../../../pch.h"

class StackArea {
public:

    StackArea(char* buffer, size_t size) :
            size(size),
            buffer(buffer)
    { };

    void* GetStart() const {
        return (void*)&buffer[0];
    };
    void* GetEnd() const {
        return (void*)&buffer[size];
    };

    const size_t size;
    const char* buffer;
};

#endif //ALLOCATORS_STACK_AREA_H
