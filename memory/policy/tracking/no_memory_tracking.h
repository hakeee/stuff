//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_NO_MEMORY_TRACKING_H
#define ALLOCATORS_NO_MEMORY_TRACKING_H

#include <stddef.h>
#include "../../source_info.h"

class NoMemoryTracking
{
public:
    inline void OnAllocation(void*, size_t, size_t, const source_info&) const {}
    inline void OnDeallocation(void*) const {}
};

#endif //ALLOCATORS_NO_MEMORY_TRACKING_H
