//
// Created by hakeee on 12/22/15.
//

#ifndef ALLOCATORS_COUNT_MEMORY_TRACKING_H
#define ALLOCATORS_COUNT_MEMORY_TRACKING_H

#include "../../../pch.h"
#include "../../source_info.h"
#include "../../../logger/logger.h"

template<class Logger>
class CountMemoryTracking
{
public:
    inline void OnAllocation(void* pointer, size_t, size_t size, const source_info& info) {
        ++m_count;
    }
    inline void OnDeallocation(void* pointer) {
        --m_count;
    }

    ~CountMemoryTracking() {
        if(m_count != 0)
            m_logger.LogLine(VERBOSE_LEVEL::DEBUG, "Leak m_count (", m_count, ")");
    }
private:
    int m_count = 0;
    Logger m_logger;
};

#endif //ALLOCATORS_COUNT_MEMORY_TRACKING_H
