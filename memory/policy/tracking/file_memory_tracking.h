//
// Created by hakeee on 12/21/15.
//

#ifndef ALLOCATORS_FILE_MEMORY_TRACKING_H
#define ALLOCATORS_FILE_MEMORY_TRACKING_H

#include "../../../pch.h"
#include "../../source_info.h"
#include "../../../logger/logger.h"

template<class Logger>
class FileMemoryTracking
{
public:
    inline void OnAllocation(void* pointer, size_t, size_t, const source_info& info) {
        m_tracks.insert(std::make_pair(pointer, info));
    }
    inline void OnDeallocation(void* pointer) {
        m_tracks.erase(pointer);
    }

    ~FileMemoryTracking() {
        for(std::pair<void*, const source_info> pair: m_tracks)
            m_logger.LogLine(VERBOSE_LEVEL::DEBUG
                    , pair.first
                    , " - ("
                    , pair.second.size
                    , ") Leak from object created at "
                    , pair.second.line
                    , ":"
                    , pair.second.linenumber
                    , "");
    }
private:
    std::unordered_map<void*, source_info> m_tracks;
    Logger m_logger;

};

#endif //ALLOCATORS_FILE_MEMORY_TRACKING_H
