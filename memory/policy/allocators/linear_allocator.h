//
// Created by hakeee on 12/21/15.
//

#ifndef ALLOCATORS_LINEAR_ALLOCATOR_H
#define ALLOCATORS_LINEAR_ALLOCATOR_H

#include <assert.h>
#include "../../../pch.h"
#include "../../../util/pointer.h"
#include "../../../logger/logger.h"

template<typename Logger>
class LinearAllocator
{
public:
    //explicit LinearAllocator(size_t size);
    LinearAllocator(void* start, void* end) :
            m_start((char *) start),
            m_end((char *) end),
            m_current((char *) start) {
    }

    void* Allocate(size_t size, size_t alignment, size_t offset) {
        // offset pointer first, align it, and offset it back
        void* sizeptr = m_current;
        reinterpret_cast<size_t&>(m_current) += sizeof(size_t);

        m_logger.Log(VERBOSE_LEVEL::FULL, "Allocation sizeptr (", sizeof(size_t), ") ");

        m_current = pointer::AlignTop(m_current + offset, alignment) - offset;

        m_logger.Log(VERBOSE_LEVEL::FULL, "alignment (", (size_t)m_current - ((size_t) sizeptr + sizeof(size_t)), ") ");

        void* userPtr = m_current;
        m_current += size;

        m_logger.Log(VERBOSE_LEVEL::FULL, "size (", (size_t)size, ") ");
        if (m_current >= m_end)
        {
            // out of memory
            return nullptr;
        }

        size_t * sizeadr = (size_t *) ((size_t) m_current - sizeof(size_t));
        *sizeadr = size;

        m_logger.Log(VERBOSE_LEVEL::FULL, "total (", (size_t) m_current - (size_t) sizeptr, ") bytes ");
        m_logger.LogLine(VERBOSE_LEVEL::FULL, "raw addr (", (size_t) sizeptr, ")");
        return userPtr;
    }


    inline void Free(void* ptr) const {

    }

    inline void Reset(void) {
        m_current = m_start;
    }

    size_t GetAllocationSize(void* pointer) const {
        return *(size_t*)((size_t)pointer - sizeof(size_t));
    }


private:
    char* m_start;
    char* m_end;
    char* m_current;
    Logger m_logger;
};

#endif //ALLOCATORS_LINEAR_ALLOCATOR_H
