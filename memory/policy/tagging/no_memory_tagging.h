//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_NO_MEMORY_TAGGING_H
#define ALLOCATORS_NO_MEMORY_TAGGING_H

class NoMemoryTagging
{
public:
    inline void TagAllocation(void*, size_t) const {}
    inline void TagDeallocation(void*, size_t) const {}
};

#endif //ALLOCATORS_NO_MEMORY_TAGGING_H
