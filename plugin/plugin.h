//
// Created by hakeee on 1/17/16.
//

#ifndef ALLOCATORS_PLUGIN_H
#define ALLOCATORS_PLUGIN_H

#include "config.h"
#include "shared_library.h"

#include <string>

// ----------------------------------------------------------------------- //

class Kernel;

// ----------------------------------------------------------------------- //

/// Representation of a plugin
class Plugin {

public:
    /// <summary>Initializes and loads a plugin</summary>
    /// <param name="filename">Filename of the plugin to load</summary>
    MYENGINE_API Plugin(const std::string& filename);
    /// <summary>Copies an existing plugin instance</summary>
    MYENGINE_API Plugin(const Plugin& other);
    /// <summary>Unloads the plugin</summary>
    MYENGINE_API ~Plugin();

    /// <summary>Queries the plugin for its expected engine version</summary>
    MYENGINE_API int getEngineVersion() const {
        return this->getEngineVersionAddress();
    }

    /// <summary>Register the plugin to a kernel</summary>
    /// <param name="kernel">Kernel the plugin should register to</param>
    MYENGINE_API void registerPlugin(Kernel& kernel) {
        this->registerPluginAddress(kernel);
    }

    /// <summary>Creates a copy of the plugin instance</summary>
    Plugin& operator=(const Plugin& other);

private:

    /// <summary>Signature for the version query function</summary>
    typedef int GetEngineVersionFunction();
    /// <summary>Signature for the plugin's registration function</summary>
    typedef void RegisterPluginFunction(Kernel&);

    /// <summary>Handle of the loaded shared library</summary>
    SharedLibrary::HandleType sharedLibraryHandle;

    /// <summary>Number of references that exist to the shared library</summary>
    size_t* referenceCount;

    /// <summary>Function to query for the expected engine version</summary>
    GetEngineVersionFunction* getEngineVersionAddress;

    /// <summary>Registers the plugin with the kernel</summary>
    RegisterPluginFunction* registerPluginAddress;

};


#endif //ALLOCATORS_PLUGIN_H
