//
// Created by hakeee on 1/17/16.
//

#ifndef ALLOCATORS_KERNEL_H
#define ALLOCATORS_KERNEL_H

#include "config.h"
#include "servers/storage.h"
#include "servers/graphics.h"
#include "servers/content.h"
#include "plugin.h"

#include <string>
#include <map>

/// <summary>The engine's core</summary>
class Kernel {

    /// <summary>Map of plugins by their associated file names</summary>
    typedef std::map<std::string, Plugin> PluginMap;

public:
    /// <summary>Accesses the storage server</summary>
    MYENGINE_API StorageServer& getStorageServer() {
        return this->storageServer;
    }

    /// <summary>Accesses the graphics server</summary>
    MYENGINE_API GraphicsServer& getGraphicsServer() {
        return this->graphicsServer;
    }

    /// <summary>Accesses the content server</summary>
    MYENGINE_API ContentServer& getContentServer() {
        return this->contentServer;
    }

    /// <summary>Loads a plugin</summary>
    /// <param name="filename">File the plugin will be loaded from</param>
    MYENGINE_API void loadPlugin(const std::string& filename) {
        if (this->loadedPlugins.find(filename) == this->loadedPlugins.end()) {
            auto& plugin = this->loadedPlugins.insert(
                    PluginMap::value_type(filename, Plugin(filename))
            ).first->second;
            if(plugin.getEngineVersion() >= GetEngineVersion())
                plugin.registerPlugin(*this);
            else {
                printf("Tried to load old plugin \"%s\" for engine version %d\n", filename.c_str(), plugin.getEngineVersion());
                this->loadedPlugins.erase(filename);
            }
        }
    }

    MYENGINE_API int GetEngineVersion() const {
        return ALLOCATOR_ENGINE_VERSION;
    }

private:
    /// <summary>All plugins currently loaded</summary>
    PluginMap loadedPlugins;

    /// <summary>Manages storage-related tasks for the engine</summary>
    StorageServer storageServer;

    /// <summary>Manages graphics-related tasks for the engine</summary>
    GraphicsServer graphicsServer;

    /// <summary>Manages content-related tasks for the engine</summary>
    ContentServer contentServer;
};

#endif //ALLOCATORS_KERNEL_H
