//
// Created by hakeee on 2/24/16.
//

#include "png.h"
#include <algorithm>

png::~png() {
    loaded = false;
}

bool png::Reload() {
    image.clear();
    extra = lodepng::State();
    width = height = 0;
    return Load(file.c_str());
}

bool png::Load(const char *file) {
    LodePNGColorType colortype = LCT_RGBA;
    unsigned bitdepth = 8;
    unsigned char *cbuffer;
    std::vector<unsigned char> buffer;


    unsigned error = lodepng::load_file(buffer, file);
    if(error) {
        std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
        return false;
    }

    error = lodepng_decode_memory(&cbuffer, &width, &height, &buffer[0], buffer.size(), colortype, bitdepth);
    if (cbuffer && !error) {
        extra.info_raw.colortype = colortype;
        extra.info_raw.bitdepth = bitdepth;
        //size_t buffersize = lodepng_get_raw_size(width, height, &extra.info_raw);
        for(size_t i = height; i > 0; --i)
            //TODO: inte alltid * 4 (RGBA), kan vara * 3 (RGB)
            image.insert(image.end(), &cbuffer[(i - 1) * width * 4], &cbuffer[i * width * 4]);
        free(cbuffer);
        loaded = true;
        return true;
    }

    if(error) {
        std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
        return false;
    }
    return false;
}

Image::ImageStructure png::GetStructure() const {
    switch (extra.info_raw.colortype) {
        case LCT_GREY: /*greyscale: 1,2,4,8,16 bit*/
            break;
        case LCT_RGB: /*RGB: 8,16 bit*/
            if(extra.info_raw.bitdepth == 8)
                return Image::ImageStructure::RGB8;
            return Image::ImageStructure::RGB16;
        case LCT_PALETTE: /*palette: 1,2,4,8 bit*/
            break;
        case LCT_GREY_ALPHA: /*greyscale with alpha: 8,16 bit*/
            break;
        case LCT_RGBA: /*RGB with alpha: 8,16 bit*/
            if(extra.info_raw.bitdepth == 8)
                return Image::ImageStructure::RGBA8;
            return Image::ImageStructure::RGBA16;
    }
    return Image::ImageStructure::RGB8;
}

const unsigned char * png::GetData() const {
    return &image[0];
}

unsigned int png::GetWidth() const {
    return width;
}

unsigned int png::GetHeight() const {
    return height;
}
