//
// Created by hakeee on 2/24/16.
//

#ifndef ALLOCATORS_PNG_H
#define ALLOCATORS_PNG_H

#include <vector>
#include "../../../servers/content.h"
#include "../../../servers/graphics.h"
#include "../loadpng.h"

class png : public Image {
public:
    ~png();
    bool Reload() override;
    bool Load(const char* file);

    ImageStructure GetStructure() const;
    const unsigned char * GetData() const;
    unsigned int GetWidth() const;
    unsigned int GetHeight() const;

    std::vector<unsigned char> image;
    unsigned width = 0, height = 0;
private:
    LodePNGState extra;
};


#endif //ALLOCATORS_PNG_H
