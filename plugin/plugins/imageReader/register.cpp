//
// Created by hakeee on 2/24/16.
//


#include "config.h"
#include "../../kernel.h"
#include "reader.h"

#define LODEPNG_COMPILE_CPP

extern "C" IMAGEREADER_API int getEngineVersion() {
    return 2;
}

extern "C" IMAGEREADER_API void registerPlugin(Kernel &kernel) {
    kernel.getContentServer().addContentReader(
            std::unique_ptr<ContentServer::ContentReader>(new Reader())
    );
}


//TODO: Change all std::cout and printf to use a logger
