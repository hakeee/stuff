//
// Created by hakeee on 2/24/16.
//

#include "reader.h"
#include "content/png.h"

Reader::~Reader() {

}

bool Reader::canOpenContent(const char *filename) {
    std::string ending = this->getEnding(filename);
    return (ending.compare(".png") == 0);
}

std::weak_ptr<Content> Reader::openContent(const char *filename, bool load) {
    if(cache.count(filename) != 0)
        return std::weak_ptr<Content>(cache.at(filename));

    Content* image = nullptr;

    auto tmp = new png();

    if(load && tmp->Load(filename))
        image = tmp;
    else
        image = tmp;


    cache[filename] = std::shared_ptr<Content>(image);

    return std::weak_ptr<Content>(cache.at(filename));
}
