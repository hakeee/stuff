//
// Created by hakeee on 2/24/16.
//

#ifndef IMAGEREADER_READER_H
#define IMAGEREADER_READER_H


#include "../../servers/content.h"

class Reader  : public ContentServer::ContentReader {
public:

    virtual ~Reader();

    bool canOpenContent(const char *filename);;

    std::weak_ptr<Content> openContent(const char *filename, bool load);;

private:

    std::unordered_map<std::string, std::shared_ptr<Content>> cache;
};


#endif //IMAGEREADER_READER_H
