//
// Created by hakeee on 2/3/16.
//

#ifndef OPENGLPLUGIN_BUFFER_H
#define OPENGLPLUGIN_BUFFER_H

#include "renderer.h"

class OpenGlConstBuffer : public ConstBuffer {
public:

    OpenGlConstBuffer(const char* name, std::vector<DataLayout> layout);

    void SetData(Renderer::ShaderPtr shader, unsigned int slot, DataLayout data) override;
    void SetData(Renderer::ShaderPtr /*shader*/, unsigned int slot, void* data) override;

    virtual void Use(Renderer::ShaderPtr shader) override;
};



#endif //OPENGLPLUGIN_BUFFER_H
