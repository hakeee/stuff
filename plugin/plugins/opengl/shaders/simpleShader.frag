#version 300 es

#ifdef GL_ES
precision highp float;
#endif

//varying vec2 st; // Texcoords

out vec4 glFragColor;


vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

float snoise(vec2 v)
  {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

// Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
		+ i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

uniform float time;
uniform sampler2D tex;

in highp vec2 frag_texcoords;

void main() {
    // Distance to nearest point in a grid of
    // (frequency x frequency) points over the unit square
    float frequency = 10.0;
    vec2 nearest = 2.0*fract(frequency * (gl_FragCoord.xy/400.0)) - 1.0;
    vec2 pos = gl_FragCoord.xy/2.1 + time;
    float dist = length(nearest);
    float radius = 0.5;
    vec3 white = vec3((snoise(pos/110.0 + 0.10)+1.0)/2.0,(snoise(pos/110.0 + 0.11)+1.0)/2.0,(snoise(pos/110.0 + 0.12)+1.0)/2.0);
    vec3 black = vec3((snoise(pos/110.0 + 0.12)+1.0)/2.0,(snoise(pos/110.0 + 0.13)+1.0)/2.0,(snoise(pos/110.0 + 0.13)+1.0)/2.0);
    vec3 fragcolor = smoothstep(white, black, vec3(1));
    glFragColor = vec4(fragcolor, 1.0 ) * vec4((snoise(pos/110.0 + 10.0)+1.0)/2.0, (snoise(pos/110.0)+1.0)/2.0, (snoise(pos/110.0 + 20.0)+1.0)/2.0, 1.0);
    //glFragColor = vec4(fragcolor, 0.2);
    //glFragColor = glFragColor * 0.2;
    glFragColor *= texture(tex, frag_texcoords).xyzw;
    //glFragColor = texture(tex, vec2(0.5,0.5));
}