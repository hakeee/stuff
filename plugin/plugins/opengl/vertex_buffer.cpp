//
// Created by hakeee on 2/27/16.
//

#include "vertex_buffer.h"

OpenGlVertexBuffer::OpenGlVertexBuffer() {
    glGenVertexArrays(1, &vertexArray);
}

OpenGlVertexBuffer::~OpenGlVertexBuffer() {
    glDeleteVertexArrays(1, &vertexArray);
}
