//
// Created by hakeee on 1/18/16.
//

#ifndef OPENGLPLUGIN_RENDERER_H
#define OPENGLPLUGIN_RENDERER_H

#include <fstream>
#include <GL/glew.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <string.h>
#include "../../kernel.h"
#include "opengl_graphics_driver.h"

#include "../../../memory/memory_area.h"
#include "../../../memory/policy/area/heap_area.h"
#include "shader.h"

class ConstBuffer;
class Opengl_renderer : public Renderer {
public:
    Opengl_renderer(OpenGLGraphicsDriver *driver, Display *display, XWindowAttributes &xWindowAttributes);
    ~Opengl_renderer();

    void Clear() const override;

    void SetClearColor(float r, float g, float b, float a) override;

    void DrawString(const char *str, const float x, const float y, const float r, const float b, const float g) override;

    void DrawFullscreenQuad() override;

    ShaderPtr CompileShader(Content* vertShader, Content* fragShader) override;
    ShaderPtr CompileShader(ShaderPtr tmp) override;
    bool IsValidShader(const ShaderPtr &shader) override;
    void DeleteShader(ShaderPtr &shader) override;
    void UseShader(ShaderPtr &shader) override;


    TexturePtr CreateTexture(std::shared_ptr<Image> image, const char* name);
    bool IsValidTexture(const TexturePtr &image);
    bool ReloadTexture(const TexturePtr &image);
    void DeleteTexture(TexturePtr &image);
    void UseTexture(Renderer::TexturePtr &image, const char *slot);
    void UseTexture(Renderer::TexturePtr &image, unsigned int id, const char *slot);


    ConstBuffer *CreateConstBuffer(const char *name, std::vector<DataLayout> layout) override;
    void DeleteBuffer(ConstBuffer *buffer) override;

    void SwapBuffer() const override;

    void Finish() override;

private:
    OpenGLGraphicsDriver *driver;

    Display *display;
    XWindowAttributes &window_attributes;

    // X11
    XFontStruct *fontinfo;
    GLuint base;
    unsigned int first, last;

    GLuint shader_in_use;

    Shader no_shader;


    typedef memory_area<LinearAllocator<ConsoleLogger>, SingleThreadPolicy, NoBoundsChecking, FileMemoryTracking<ConsoleLogger>, NoMemoryTagging> LinearCountMemArea;

    /*//TODO: make objectpool
    HeapArea shader_heap;
    LinearCountMemArea shader_area;


    HeapArea buffer_heap;
    LinearCountMemArea buffer_area;*/

    ConsoleLogger logger;
};

#endif //OPENGLPLUGIN_RENDERER_H
