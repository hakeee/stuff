//
// Created by hakeee on 2/21/16.
//

#ifndef ALLOCATORS_FRAG_H
#define ALLOCATORS_FRAG_H


#include <GL/glew.h>
#include "../../../servers/content.h"
#include "../../../../pch.h"

class Shader;
class frag : public Content {
public:
    ~frag();

    bool Reload();
    bool Compile();

private:
    GLuint  shaderId = 0;
    Shader* shader = nullptr;

    ConsoleLogger       logger;
    friend class Shader;
};


#endif //ALLOCATORS_FRAG_H
