//
// Created by hakeee on 2/21/16.
//

#include <fstream>
#include "frag.h"
#include "../shader.h"
#include "../../../../logger/logger.h"

frag::~frag() {
    if(shaderId != 0) {
        glDeleteShader(shaderId);
        shaderId = 0;
    }
}

bool frag::Reload() {
    loaded = false;
    return shader->Compile();
}

bool frag::Compile() {
    loaded = false;
    std::ifstream in;
    in.open(file, std::ios_base::in);

    if (!in.is_open()) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "Couldn't open file at \"", file, "\"");
        return false;
    }

    std::string shaderSource;
    shaderSource.assign(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());
    in.close();

    shaderId = glCreateShader(GL_FRAGMENT_SHADER);
    if (shaderId == 0) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "GL Error: ", glGetError(), " in ", file);
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "glCreateShader failed!");
        return false;
    }

    const GLchar *shaderSourceChar = shaderSource.c_str();
    glShaderSource(shaderId, 1, &shaderSourceChar, 0);
    //printf("GL Error: %i\n",glGetError());
    glCompileShader(shaderId);

    //printf("GL Error: %i\n",glGetError());
    GLint shaderCompiled = GL_FALSE;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &shaderCompiled);

    //printf("GL Error: %i\n",glGetError());
    if (shaderCompiled != GL_TRUE) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "GL Error: ", glGetError(), " in ", file);
        GLint maxLength = 0;
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

        GLint logLength = 0;
        GLchar *log = new char[maxLength];
        glGetShaderInfoLog(shaderId, maxLength, &logLength, log);

        if (logLength >= 0)
            logger.LogLine(VERBOSE_LEVEL::DEBUG, log);

        delete[] log;
        glDeleteShader(shaderId);
        shaderId = 0;
        return false;
    }
    loaded = true;
    return true;
}
