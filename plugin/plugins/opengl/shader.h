//
// Created by hakeee on 2/21/16.
//

#ifndef ALLOCATORS_SHADER_H
#define ALLOCATORS_SHADER_H

#include <GL/glew.h>
#include <string>
#include <vector>
#include "../../../pch.h"

class vert;
class frag;
struct Shader {

    ~Shader();


    bool Compile();

    bool IsShadersValid();
    bool IsCompiled();

    GLuint program = 0;
    bool loaded = false;

    vert* vertexShader = nullptr;
    frag* fragmentShader = nullptr;

    std::string name;
    ConsoleLogger       logger;
};


#endif //ALLOCATORS_SHADER_H
