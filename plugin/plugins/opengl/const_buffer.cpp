//
// Created by hakeee on 2/3/16.
//

#include "const_buffer.h"


OpenGlConstBuffer::OpenGlConstBuffer(const char* name, std::vector<DataLayout> layout ) : ConstBuffer(name, layout) { }

void OpenGlConstBuffer::SetData(Renderer::ShaderPtr /*shader*/, unsigned int slot, DataLayout data) {
    if(slot >= layout.size()) {
        //TODO: Take care of this, log it
        return;
    }
    layout[slot] = data;
}

void OpenGlConstBuffer::SetData(Renderer::ShaderPtr /*shader*/, unsigned int slot, void* data) {
    if(slot >= layout.size()) {
        //TODO: Take care of this, log it
        return;
    }
    layout[slot].data = data;
}

void OpenGlConstBuffer::Use(Renderer::ShaderPtr shader) {
    auto location = glGetUniformLocation(reinterpret_cast<Shader *>(shader)->program, name.c_str());
    for(size_t i = 0; i < layout.size(); i++) {
        switch (layout[i].type) {
            case DataLayoutType::FLOAT:
                glUniform1fv(location, layout[i].count, static_cast<GLfloat*>(layout[i].data));
                break;
            case DataLayoutType::FLOAT2:
                glUniform2fv(location, layout[i].count, static_cast<GLfloat*>(layout[i].data));
                break;
            case DataLayoutType::FLOAT3:
                glUniform3fv(location, layout[i].count, static_cast<GLfloat*>(layout[i].data));
                break;
            case DataLayoutType::FLOAT4:
                glUniform4fv(location, layout[i].count, static_cast<GLfloat*>(layout[i].data));
                break;
            case DataLayoutType::INT:
                glUniform1iv(location, layout[i].count, static_cast<GLint*>(layout[i].data));
                break;
            case DataLayoutType::INT2:
                glUniform2iv(location, layout[i].count, static_cast<GLint*>(layout[i].data));
                break;
            case DataLayoutType::INT3:
                glUniform3iv(location, layout[i].count, static_cast<GLint*>(layout[i].data));
                break;
            case DataLayoutType::INT4:
                glUniform4iv(location, layout[i].count, static_cast<GLint*>(layout[i].data));
                break;
        }
    }
}
