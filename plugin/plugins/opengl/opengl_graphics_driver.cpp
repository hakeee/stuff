//
// Created by hakeee on 1/17/16.
//

#include <assert.h>
#include <X11/Xutil.h>
#include "opengl_graphics_driver.h"
#include "renderer.h"

/// OpenGL graphics drver

OpenGLGraphicsDriver::OpenGLGraphicsDriver() {

}

/// <summary>Destroys an OpenGL graphics driver</summary>
OpenGLGraphicsDriver::~OpenGLGraphicsDriver() {
    //TODO: deallocate and close driver

    XUnmapWindow(display, frame_window);
    glXMakeCurrent(display, None, NULL);
    renderer.reset();
    XFreeColormap(display, colormap);
    glXDestroyContext(display, glx_context);
    XDestroyWindow(display, frame_window);
    XFree(visual_info);
    XCloseDisplay(display);
    display = nullptr;
}

/// <summary>Gets the name of the graphics driver</summary>
const std::string& OpenGLGraphicsDriver::getName() const {
    static std::string sName("OpenGL graphics driver");
    return sName;
}

/// <summary>Creates a renderer</summary>
Renderer & OpenGLGraphicsDriver::createRenderer(unsigned int width, unsigned int height) {
    XInitThreads();
    display = XOpenDisplay(NULL);
    const char * xserver = getenv("DISPLAY");

    if (display == 0)
    {
        printf("Could not establish a connection to X-server '%s'\n", xserver );
        exit(1);
    }

    depth  = DefaultDepth(display, 0);

    memset(&frame_attributes,0,sizeof(XSetWindowAttributes));

    frame_attributes.background_pixel = XWhitePixel(display, 0);


    int attributes[]{GLX_RGBA,
                     GLX_DEPTH_SIZE, 24,
                     GLX_DOUBLEBUFFER,
                     None};
    visual_info = glXChooseVisual(display, 0, attributes);

    if(visual_info == nullptr) {
        fprintf(stderr, "No appropiete visual found");
        assert(visual_info == nullptr);
    }

    colormap = XCreateColormap(display, RootWindow(display, 0), visual_info->visual, AllocNone);
    frame_attributes.colormap = colormap;

    frame_attributes.background_pixmap = None;
    frame_attributes.border_pixel      = 0;

    /* create the application window */
    frame_window = XCreateWindow(display, RootWindow(display, 0),
                                 0, 0, width, height, 0, depth,
                                 InputOutput, visual_info->visual, CWColormap | CWEventMask | CWBorderPixel | CWBackPixmap,
                                 &frame_attributes);

    XStoreName(display, frame_window, "The Game of Life");


    glx_context = glXCreateContext(display, visual_info, NULL, GL_TRUE);
    glXMakeCurrent(display, frame_window, glx_context);

    GLenum glew = glewInit();
    if(glew != GLEW_OK)
        std::cerr << "Glew Error:" << glewGetErrorString(glew) << std::endl;

    XSelectInput(display, frame_window, ExposureMask | StructureNotifyMask | KeyPressMask | KeyReleaseMask);
    XGetWindowAttributes(display, frame_window, &window_attributes);

    renderer = std::unique_ptr<Opengl_renderer>(new Opengl_renderer(this, display, window_attributes));

    XMapWindow(display, frame_window);

    return *renderer.get();
}

/// <summary>Register the plugin to an engine kernel</summary>
Renderer& OpenGLGraphicsDriver::getRenderer() const {
    assert(renderer != nullptr);
    return *renderer.get();
}

/// <summary>Retrieve the engine version we're going to expect</summary>
bool OpenGLGraphicsDriver::isKeyDown(char key) {
    return keys[XKeysymToKeycode(display, (KeySym) key /*XK_x*/)];
}

/// <summary>Runs one event message for the created window</summary>
bool OpenGLGraphicsDriver::executeNextEvent(bool draw) {
    glXMakeContextCurrent(display, frame_window, frame_window, glx_context);
    bool redraw = draw;
    while(XPending(display) > 0) {
        XNextEvent(display, &event);
        switch (event.type) {
            case Expose: {
                redraw = true;
                break;
            }
            case KeyPress: {
                keys[event.xkey.keycode] = true;
                break;
            }
            case KeyRelease: {
                keys[event.xkey.keycode] = false;
                break;
            }
            case ConfigureNotify:
                XGetWindowAttributes(display, frame_window, &window_attributes);
                glViewport(0, 0, window_attributes.width, window_attributes.height);
            default:
                break;
        }
    }
    if(redraw && exposeCallback)
        exposeCallback(this);
    return true;
};
