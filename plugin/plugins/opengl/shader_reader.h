//
// Created by hakeee on 2/23/16.
//

#ifndef ALLOCATORS_SHADER_READER_H
#define ALLOCATORS_SHADER_READER_H

#include "opengl_graphics_driver.h"

class ShaderReader : public ContentServer::ContentReader {
public:

    virtual ~ShaderReader();

    bool canOpenContent(const char *filename);;

    std::weak_ptr<Content> openContent(const char *filename, bool load);;

private:

    std::unordered_map<std::string, std::shared_ptr<Content>> cache;
};


#endif //ALLOCATORS_SHADER_READER_H
