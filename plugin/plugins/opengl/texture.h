//
// Created by hakeee on 2/25/16.
//

#ifndef ALLOCATORS_TEXTURE_H
#define ALLOCATORS_TEXTURE_H


#include <GL/glew.h>
#include <string>
#include "../../servers/graphics.h"

class Texture {
public:
    Texture();
    ~Texture();
    bool Reload();
    bool IsTextureValid();

    GLuint id = 0;
    bool loaded = false;
    std::string name;
    std::shared_ptr<Image> image;
};


#endif //ALLOCATORS_TEXTURE_H
