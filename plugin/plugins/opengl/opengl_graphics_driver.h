//
// Created by hakeee on 1/25/16.
//

#ifndef OPENGLPLUGIN_OPENGL_PLUGIN_H
#define OPENGLPLUGIN_OPENGL_PLUGIN_H

#include <GL/glew.h>
#include <GL/glx.h>
#include <X11/Xlib.h>
#include <memory>
#include <X11/Xutil.h>
#include <unordered_map>

#include "config.h"
#include "../../kernel.h"

class Opengl_renderer;

/// OpenGL graphics drver
class OpenGLGraphicsDriver : public GraphicsServer::GraphicsDriver {

public:
    OPENGLPLUGIN_API OpenGLGraphicsDriver();

    /// <summary>Destroys an OpenGL graphics driver</summary>
    OPENGLPLUGIN_API virtual ~OpenGLGraphicsDriver() override;

    /// <summary>Gets the name of the graphics driver</summary>
    OPENGLPLUGIN_API virtual const std::string& getName() const override;

    /// <summary>Creates a renderer</summary>
    Renderer &createRenderer(unsigned int width, unsigned int height) override;
    Renderer& getRenderer() const override;

    /// <summary>Runs one event message for the created window</summary>
    bool executeNextEvent(bool draw = false) override;
    bool isKeyDown(char key) override;

private:
    Display                 *display = nullptr;
    XEvent                  event;
    XVisualInfo             *visual_info;
    Colormap                colormap;
    int                     depth;
    XSetWindowAttributes    frame_attributes;
    Window                  frame_window;
    XWindowAttributes       window_attributes;
    GLXContext              glx_context;

    std::unique_ptr<Opengl_renderer> renderer;

    std::unordered_map<unsigned int, bool> keys;

    friend class Opengl_renderer;
};

#endif //ALLOCATORS_OPENGL_PLUGIN_H
