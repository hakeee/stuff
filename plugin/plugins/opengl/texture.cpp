//
// Created by hakeee on 2/25/16.
//

#include "texture.h"

Texture::Texture() {
    glGenTextures(1, &id);
}

Texture::~Texture() {
    glDeleteTextures(1, &id);
}

bool Texture::Reload() {
    loaded = false;
    if(image) {
        image->Reload();

        glBindTexture(GL_TEXTURE_2D, id);

        // Give the image to OpenGL

        //for(unsigned int i = 0; i < image->GetWidth()* image->GetHeight(); i++) {
            //if(image->GetData()[i] < 255)
                //printf("something\n");
        //}


        if(image->GetStructure() == Image::ImageStructure::RGB8)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->GetWidth(), image->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, image->GetData());
        else if(image->GetStructure() == Image::ImageStructure::RGBA8)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->GetWidth(), image->GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, image->GetData());

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        loaded = true;
        return true;
    }
    return false;
}

bool Texture::IsTextureValid() {
    return id != 0 && image && image->loaded && loaded;
}
