//
// Created by hakeee on 2/3/16.
//

#include <X11/Xlib.h>
#include "renderer.h"
#include "const_buffer.h"
#include "content/vert.h"
#include "content/frag.h"
#include "texture.h"

Opengl_renderer::Opengl_renderer(OpenGLGraphicsDriver *driver, Display *display, XWindowAttributes &xWindowAttributes)
        : driver(driver), display(display), window_attributes(xWindowAttributes),
          shader_in_use(0)/*,

          shader_heap(100 * sizeof(Shader)), shader_area(shader_heap),
          buffer_heap(100 * sizeof(OpenGlConstBuffer)), buffer_area(buffer_heap)*/ {

    no_shader.loaded = true;
    no_shader.program = 0;

    NoShader = reinterpret_cast<ShaderPtr>(&no_shader);

    Font id;
    fontinfo = XLoadQueryFont(display,
//                              "-adobe-times-medium-r-normal--17-120-100-100-p-88-iso8859-1"
                                "10x20"
    );

    if (fontinfo == NULL) {
        printf("no font found\n");
        exit(0);
    }

    id = fontinfo->fid;
    first = fontinfo->min_char_or_byte2;
    last = fontinfo->max_char_or_byte2;

    base = glGenLists(last + 1);
    if (base == 0) {
        printf("out of display lists\n");
        exit(0);
    }
    glXUseXFont(id, first, last - first + 1, base + first);
}

Opengl_renderer::~Opengl_renderer() {
    glDeleteLists(base, last - first + 1);
    XFreeFont(display, fontinfo);
}

void Opengl_renderer::Clear() const {
    glClear(GL_COLOR_BUFFER_BIT);
}

void Opengl_renderer::SetClearColor(float r, float g, float b, float a) {
    glClearColor(r, g, b, a);
}

void Opengl_renderer::DrawString(const char *str, const float x, const float y, const float r, const float b, const float g) {
    glColor3f(r,b,g);
    glRasterPos2f(x, y);
    glPushAttrib(GL_LIST_BIT);                                  // Pushes The Display List Bits
    glListBase(base);                                           // Sets The Base Character to 32
    glCallLists((GLsizei) strlen(str), GL_UNSIGNED_BYTE, str);  // Draws The Display List Text
    glPopAttrib();
}

void Opengl_renderer::DrawFullscreenQuad() {
    glBegin(GL_TRIANGLE_STRIP);
    glVertex3f(-1.0f, -1.0f, 0.0f); //vertex 1
    glVertex3f(-1.0f, 1.0f, 0.0f);  //vertex 2
    glVertex3f(1.0f, -1.0f, 0.0f);  //vertex 3
    glVertex3f(1.0f, 1.0f, 0.0f);   //vertex 4
    glEnd();
}

Renderer::ShaderPtr Opengl_renderer::CompileShader(Content* vertShader, Content* fragShader) {
    Shader *tmp = new Shader;//static_cast<Shader *>(shader_area.allocate(sizeof(Shader), 0,
                  //                                              source_info(__FILE__, __LINE__, sizeof(Shader))));

    tmp->vertexShader = dynamic_cast<vert*>(vertShader);
    tmp->fragmentShader = dynamic_cast<frag*>(fragShader);
    tmp->Compile();

    return CompileShader(reinterpret_cast<ShaderPtr>(tmp));
}

Renderer::ShaderPtr Opengl_renderer::CompileShader(ShaderPtr tmpPtr) {
    Shader* tmp = reinterpret_cast<Shader*>(tmpPtr);
    tmp->Compile();
    return reinterpret_cast<ShaderPtr>(tmp);
}

bool Opengl_renderer::IsValidShader(const ShaderPtr &shader) {
    return reinterpret_cast<Shader *>(shader)->IsShadersValid();
};

void Opengl_renderer::DeleteShader(ShaderPtr &shader) {
    delete reinterpret_cast<Shader *>(shader);
    shader = 0;
};

void Opengl_renderer::UseShader(ShaderPtr &shader) {
    Shader * shaderTmp = reinterpret_cast<Shader *>(shader);
    shader_in_use = shaderTmp->program;
    glUseProgram(shader_in_use);
}

ConstBuffer *Opengl_renderer::CreateConstBuffer(const char *name, std::vector<DataLayout> layout) {
    return new OpenGlConstBuffer(name, layout);
}

void Opengl_renderer::DeleteBuffer(ConstBuffer *buffer) {
    delete buffer;
}

void Opengl_renderer::SwapBuffer() const {
    glXSwapBuffers(display, driver->frame_window);
}

void Opengl_renderer::Finish() {
    glFinish();
}

Renderer::TexturePtr Opengl_renderer::CreateTexture(std::shared_ptr<Image> image, const char* name) {
    Texture* tmp = new Texture();
    tmp->image = image;
    tmp->name = name;
    if(tmp->Reload())
        return reinterpret_cast<TexturePtr>(tmp);
    return 0;
}

bool Opengl_renderer::IsValidTexture(const Renderer::TexturePtr &image) {
    return reinterpret_cast<Texture*>(image)->IsTextureValid();
}

bool Opengl_renderer::ReloadTexture(const Renderer::TexturePtr &image) {
    return reinterpret_cast<Texture*>(image)->Reload();
}

void Opengl_renderer::DeleteTexture(Renderer::TexturePtr &image) {
    delete reinterpret_cast<Texture*>(image);
}

void Opengl_renderer::UseTexture(Renderer::TexturePtr &image, const char *slot) {
    glBindTexture(GL_TEXTURE_2D, reinterpret_cast<Texture*>(image)->id);
    glUniform1i(glGetUniformLocation(shader_in_use, slot), reinterpret_cast<Texture*>(image)->id);
}

void Opengl_renderer::UseTexture(Renderer::TexturePtr &image, unsigned int id, const char *slot) {
    auto texid = reinterpret_cast<Texture*>(image)->id;
    glActiveTexture(GL_TEXTURE0 + id);
    glBindTexture(GL_TEXTURE_2D, texid);
    auto location = glGetUniformLocation(shader_in_use, slot);
    glUniform1i(location, id);
    //printf((const char *) glewGetErrorString(glGetError()));
    glActiveTexture(GL_TEXTURE0);
}
