//
// Created by hakeee on 2/27/16.
//

#ifndef ALLOCATORS_VERTEX_BUFFER_H
#define ALLOCATORS_VERTEX_BUFFER_H


#include <GL/glew.h>

class OpenGlVertexBuffer {
public:
    OpenGlVertexBuffer();
    ~OpenGlVertexBuffer();

    //TODO: gör något smart med vertexArray och input layout / vertexArray kanske ska ligga i shader

private:

    GLuint id, vertexArray;
};


#endif //ALLOCATORS_VERTEX_BUFFER_H
