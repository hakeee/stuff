//
// Created by hakeee on 2/12/16.
//


#define OPENGLPLUGIN_SOURCE 1

#include "opengl_graphics_driver.h"
#include "shader_reader.h"

extern "C" OPENGLPLUGIN_API int getEngineVersion() {
    return 2;
}

extern "C" OPENGLPLUGIN_API void registerPlugin(Kernel &kernel) {
    kernel.getGraphicsServer().addGraphicsDriver(
            std::unique_ptr<GraphicsServer::GraphicsDriver>(new OpenGLGraphicsDriver())
    );
    kernel.getContentServer().addContentReader(
            std::unique_ptr<ContentServer::ContentReader>(new ShaderReader())
    );
}