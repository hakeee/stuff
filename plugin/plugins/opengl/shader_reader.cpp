//
// Created by hakeee on 2/23/16.
//

#include "shader_reader.h"
#include "content/vert.h"
#include "content/frag.h"

ShaderReader::~ShaderReader() { }

bool ShaderReader::canOpenContent(const char *filename) {
    std::string ending = this->getEnding(filename);
    return (ending.compare(".vert") == 0 || ending.compare(".frag") == 0);
}

std::weak_ptr<Content> ShaderReader::openContent(const char *filename, bool load) {
    if(cache.count(filename) != 0)
        return std::weak_ptr<Content>(cache.at(filename));

    std::string ending = this->getEnding(filename);
    Content* shader = nullptr;
    if(ending.compare(".vert") == 0) {
        auto v = new vert();
        if(load)
            v->Reload();
        shader = v;
    } else if(ending.compare(".frag") == 0) {
        auto f = new frag();
        if(load)
            f->Reload();
        shader = f;
    }

    if(shader != nullptr) {
        cache[filename] = std::shared_ptr<Content>(shader);
    }

    return std::weak_ptr<Content>(cache.at(filename));
}
