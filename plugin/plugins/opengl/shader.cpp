//
// Created by hakeee on 2/21/16.
//

#include "shader.h"
#include "../../../logger/logger.h"
#include "content/vert.h"
#include "content/frag.h"

Shader::~Shader() {
    if(program != 0) {
        glDeleteProgram(program);
        program = 0;
    }
}

bool Shader::Compile() {
    loaded = false;
    if(program != 0)
        glDeleteProgram(program);

    program = glCreateProgram();
    if (program == 0) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "GL Error: ", glGetError(), " in ", name);
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "Couldn't create shader program!");
        return false;
    }

    if(!vertexShader->loaded && !vertexShader->Compile()) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "Couldn't create shader program!");

        return false;
    }
    if(!fragmentShader->loaded && !fragmentShader->Compile()) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "Couldn't create shader program!");

        return false;
    }
    glAttachShader(program, vertexShader->shaderId);
    glAttachShader(program, fragmentShader->shaderId);
    glLinkProgram(program);
    //glUseProgram(program_);

    GLint linkSuccess = GL_TRUE;
    glGetProgramiv(program, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess != GL_TRUE) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "GL Error: ", glGetError(), " in ", name);
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        GLint logLength = 0;
        GLchar *log = new char[maxLength];
        glGetShaderInfoLog(program, maxLength, &logLength, log);
        if (logLength > 0)
            logger.LogLine(VERBOSE_LEVEL::DEBUG, log);
        else
            logger.LogLine(VERBOSE_LEVEL::DEBUG,
                           "An error occured while linking shader program but no log was available!");

        delete[] log;

        if(program != 0) {
            glDeleteProgram(program);
            program = 0;
        }
        return false;
    }


    loaded = true;
    return true;
}



bool Shader::IsShadersValid() {
    return vertexShader->loaded && fragmentShader->loaded;
}

bool Shader::IsCompiled() {
    return this->loaded;
}