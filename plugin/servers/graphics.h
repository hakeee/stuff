//
// Created by hakeee on 1/17/16.
//

#ifndef ALLOCATORS_GRAPHICS_H
#define ALLOCATORS_GRAPHICS_H

#include "../config.h"
#include "content.h"

#include <vector>
#include <string>
#include <functional>
#include <memory>

// ----------------------------------------------------------------------- //
class ConstBuffer;

enum class DataLayoutType {
    FLOAT,
    FLOAT2,
    FLOAT3,
    FLOAT4,
    INT,
    INT2,
    INT3,
    INT4
};
struct DataLayout {
    DataLayoutType type = DataLayoutType::FLOAT;
    union {
        void *data = nullptr;
        size_t offset;
    };
    unsigned int count = 1;
};

class Image : public Content {
public:
    enum class ImageStructure {
        RGBA8,
        BGRA8,
        RGBA16,
        BGRA16,
        RGB8,
        BGR8,
        RGB16,
        BGR16
    };

    virtual ~Image() { }
    virtual ImageStructure GetStructure() const = 0;
    virtual const unsigned char * GetData() const = 0;
    virtual unsigned int GetWidth() const = 0;
    virtual unsigned int GetHeight() const = 0;
};

class Renderer {
public:
    Renderer(): NoShader(0) {};
    // clears the background
    virtual void Clear() const = 0;
    // set clearcolor
    virtual void SetClearColor(float r, float g, float b, float a) = 0;
    // draw string
    virtual void DrawString(const char *str, const float x, const float y, const float r, const float b, const float g) = 0;

    //virtual void DrawPixel(const int x, const int y, const unsigned char r, const unsigned char g, const unsigned char b) = 0;

    virtual void DrawFullscreenQuad() = 0;
    typedef size_t ShaderPtr;
    virtual ShaderPtr CompileShader(Content* vertShader, Content* fragShader) = 0;
    virtual ShaderPtr CompileShader(ShaderPtr tmp) = 0;
    virtual bool IsValidShader(const ShaderPtr &shader) = 0;
    virtual void DeleteShader(ShaderPtr &shader) = 0;

    virtual void UseShader(ShaderPtr &shader) = 0;
    typedef size_t TexturePtr;
    virtual TexturePtr CreateTexture(std::shared_ptr<Image> image, const char* name) = 0;
    virtual bool IsValidTexture(const TexturePtr &image) = 0;
    virtual bool ReloadTexture(const TexturePtr &image) = 0;
    virtual void DeleteTexture(TexturePtr &image) = 0;
    virtual void UseTexture(Renderer::TexturePtr &image, const char *slot) = 0;
    virtual void UseTexture(Renderer::TexturePtr &image, unsigned int id, const char *slot) = 0;

    virtual ConstBuffer *CreateConstBuffer(const char *name, std::vector<DataLayout> layout) = 0;
    //TODO: virtual VertexBuffer *CreateVertexBuffer(const char *name, std::vector<DataLayout> layout) = 0;
    virtual void DeleteBuffer(ConstBuffer *buffer) = 0;


    virtual void SwapBuffer() const = 0;

    virtual void Finish() = 0;
    ShaderPtr NoShader;


}; // Interface


class ConstBuffer {
public:
    ConstBuffer(const char* name, std::vector<DataLayout> layout):
            name(name),
            layout(layout)
    { }
    virtual ~ConstBuffer() { }
    
    // update data already in buffer
    virtual void Use(Renderer::ShaderPtr shader) = 0;
    // set new data
    virtual void SetData(Renderer::ShaderPtr shader, unsigned int slot, DataLayout data) = 0;
    virtual void SetData(Renderer::ShaderPtr shader, unsigned int slot, void* data) = 0;

protected:

    std::string name;
    std::vector<DataLayout> layout;
};

// ----------------------------------------------------------------------- //

/// <summary>Manages graphics related stuff for the engine</summary>
class GraphicsServer {
public:

    /// <summary>Graphics driver interface for the engine</summary>
    class GraphicsDriver {
    public:
        using ExposeCallback_t = std::function<void(GraphicsServer::GraphicsDriver* graphicsServer)>;
        /// <summary>Releases resources of a driver after use</summary>
        virtual ~GraphicsDriver() {}
        /// <summary>Gets the name of the graphics driver</summary>
        virtual const std::string &getName() const = 0;
        /// <summary>Creates a renderer using the driver's rendering API</summary>
        virtual Renderer &createRenderer(unsigned int width, unsigned int height) = 0;
        /// <summary>Get a renderer using the driver's rendering API</summary>
        virtual Renderer& getRenderer() const = 0;
        /// <summary>Runs one event message for the created window<, returns false for exit/summary>
        virtual bool executeNextEvent(bool draw = false) = 0;

        virtual bool isKeyDown(char key) = 0;

        /// <summary>A callback for expose events</summary>
        void setExposeCallback(GraphicsServer::GraphicsDriver::ExposeCallback_t callback) {
            exposeCallback = callback;
        }

    protected:
        ExposeCallback_t exposeCallback;
    };

private:

    /// <summary>A vector of graphics drivers</summary>
    typedef std::vector<GraphicsDriver *> GraphicsDriverVector;

public:

    /// <summary>Releases the resources of the graphics server</summary>
    MYENGINE_API ~GraphicsServer() {
        for(
                GraphicsDriverVector::const_iterator it = this->graphicsDrivers.begin();
                it != this->graphicsDrivers.end();
                ++it
                ) {
            delete *it;
        }
    }

    /// <summary>Allows plugins to add new graphics drivers</summary>
    /// <param name="graphicsDriver">Graphics driver that will be added</param>
    MYENGINE_API void addGraphicsDriver(
            std::unique_ptr<GraphicsDriver> graphicsDriver
    ) {
        this->graphicsDrivers.push_back(graphicsDriver.release());
    }

    /// <summary>Gets the total number of registered graphics drivers</summary>
    MYENGINE_API size_t getDriverCount() const {
        return this->graphicsDrivers.size();
    }

    /// <summary>Accesses a driver by its index</summary>
    MYENGINE_API GraphicsDriver &getDriver(size_t Index) {
        return *this->graphicsDrivers.at(Index);
    }

    /// <summary>All available graphics drivers</summary>
private:
    GraphicsDriverVector graphicsDrivers;

};

#endif //ALLOCATORS_GRAPHICS_H
