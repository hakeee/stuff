//
// Created by hakeee on 1/17/16.
//

#ifndef ALLOCATORS_STORAGE_H
#define ALLOCATORS_STORAGE_H

#include "../config.h"

#include <list>
#include <string>
#include <memory>
#include <stdexcept>

// ----------------------------------------------------------------------- //

class Archive {}; // Dummy

// ----------------------------------------------------------------------- //

/// <summary>Manages storage related stuff</summary>
class StorageServer {

public:
    /// <summary>Reads archive files like zips and rars</summary>
    class ArchiveReader {
    public:
        /// <summary>Releases any resources owned by the reader</summary>
        virtual ~ArchiveReader() {}

        /// <summary>Checks whether the reader can open an archive</summary>
        /// <param name="filename">Name of the file that will be checked</param>
        virtual bool canOpenArchive(const std::string &filename) = 0;

        /// <summary>Opens an archive for reading</summary>
        /// <param name="filename">Filename of the archive to open</param>
        virtual std::unique_ptr<Archive> openArchive(
                const std::string &filename
        ) = 0;
    };

private:
    /// <summary>A list of archive readers</summary>
    typedef std::list<ArchiveReader *> ArchiveReaderList;

public:

    /// <summary>Destroys the storage server</summary>
    MYENGINE_API ~StorageServer() {
        for(
                ArchiveReaderList::reverse_iterator it = this->archiveReaders.rbegin();
                it != this->archiveReaders.rend();
                ++it
                ) {
            delete *it;
        }
    }

    /// <summary>Allows plugins to add new archive readers</summary>
    MYENGINE_API void addArchiveReader(
            std::unique_ptr<ArchiveReader> archiveReader
    ) {
        this->archiveReaders.push_back(archiveReader.release());
    }

    /// <summry>
    ///   Opens an archive by searching for a matching archive reader
    /// </summary>
    /// <param name="filename">File a reader will be searched for</param>
    MYENGINE_API std::unique_ptr<Archive> openArchive(
            const std::string &filename
    ) {
        for(
                ArchiveReaderList::iterator it = this->archiveReaders.begin();
                it != this->archiveReaders.end();
                ++it
                ) {
            if((*it)->canOpenArchive(filename))
                return (*it)->openArchive(filename);
        }

        throw std::runtime_error("Invalid or unsupported archive type");
    }

private:
    ArchiveReaderList archiveReaders; ///< All available archive readers

};

#endif //ALLOCATORS_STORAGE_H
