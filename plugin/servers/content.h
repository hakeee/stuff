//
// Created by hakeee on 2/12/16.
//

#ifndef ALLOCATORS_CONTENT_H
#define ALLOCATORS_CONTENT_H

#include "../config.h"
#include "../../directory/file_watcher.h"

#include <list>
#include <string>
#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <thread>

// ----------------------------------------------------------------------- //

class Content {
public:
    virtual ~Content() {};
    virtual bool Reload() { return true; };
    bool loaded = false;
    std::string file;
    size_t filehash;
}; // Dummy


// ----------------------------------------------------------------------- //


/// <summary>Manages storage related stuff</summary>
class ContentServer {

public:
    /// <summary>Reads Content files like bmp and png</summary>
    class ContentReader {
    public:
        /// <summary>Releases any resources owned by the reader</summary>
        virtual ~ContentReader() { }

        /// <summary>Checks whether the reader can open an Content</summary>
        /// <param name="filename">Name of the file that will be checked</param>
        virtual bool canOpenContent(const char *filename) = 0;

        /// <summary>Opens an Content for reading</summary>
        /// <param name="filename">Filename of the Content to open</param>
        virtual std::weak_ptr<Content> openContent(const char *filename, bool load) = 0;

    protected:
        std::string getEnding(std::string file) const {
            auto pos = file.find_last_of(".");
            if (pos != file.npos)
                return file.substr(pos).c_str();
            else
                return "";
        }
    };

private:
    /// <summary>A list of Content readers</summary>
    typedef std::list<ContentReader *> ContentReaderList;

public:

    /// <summary>Destroys the storage server</summary>
    MYENGINE_API ~ContentServer() {
        stopWatching();
        for (auto it = this->contentReaders.rbegin();
                it != this->contentReaders.rend();
                ++it) {
            delete *it;
        }
    }

    /// <summary>Allows plugins to add new Content readers</summary>
    MYENGINE_API void addContentReader(
            std::unique_ptr<ContentReader> ContentReader
    ) {
        this->contentReaders.push_back(ContentReader.release());
    }

    /// <summry>
    ///   Opens an Content by searching for a matching Content reader
    /// </summary>
    /// <param name="filename">File a reader will be searched for</param>
    std::weak_ptr<Content> getContent(const std::string &filename, bool load, bool addwatch) {
        for (auto it = this->contentReaders.begin();
                it != this->contentReaders.end();
                ++it) {
            if ((*it)->canOpenContent(filename.c_str())) {
                auto ret = (*it)->openContent(filename.c_str(), load);
                ret.lock()->file = filename;
                ret.lock()->filehash = std::hash<std::string>()(filename);
                if(addwatch)
                {
                    watcher.add(filename.c_str());
                }
                return ret;
            }
        }

        return std::weak_ptr<Content>(no_content);
        //throw std::runtime_error("Invalid or unsupported Content type");
    }

    void startWatching(bool /*block*/, bool reload) {
        watcher.m_change_callback = [this, reload](const char* file, uint32_t /*mask*/) {
            std::string str = file;
            std::cout << "File updated: \"" << str << "\"" << std::endl;
            if(str.size() > 0 && str.find("__") == str.npos ) {

                auto content = getContent(file, reload, false);
                content.lock().get()->loaded = false;
                if (reload) content.lock().get()->Reload();
            }
        };
        watchFunction();
    }

    void stopWatching() {
        watcher.stop_watch();
        if(watcherThread != nullptr) {
            watcherThread->join();
            delete watcherThread;
        }
    }

private:
    void watchFunction() {
        if(watcherThread == nullptr)
            watcherThread = new std::thread([&]() {
                watcher.start_watch();
            });
    }

    ContentReaderList   contentReaders; ///< All available Content readers
    std::shared_ptr<Content> no_content;
    file_watcher        watcher;
    std::thread         *watcherThread = nullptr;

};

#endif //ALLOCATORS_CONTENT_H
