//
// Created by hakeee on 12/31/15.
//

#ifndef ALLOCATORS_TIMER_H
#define ALLOCATORS_TIMER_H


#include <chrono>
#include <iostream>

class timer {
public:

    void start() {
        begin = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    }

    long int time_since_start() {
        return std::chrono::high_resolution_clock::now().time_since_epoch().count() - begin;
    }

private:

    long int begin = 0;
};

#endif //ALLOCATORS_TIMER_H
