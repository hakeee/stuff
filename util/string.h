//
// Created by hakeee on 1/15/16.
//

#ifndef ALLOCATORS_STRING_H
#define ALLOCATORS_STRING_H

#include <string>
#include <sstream>
#include <vector>
#include <memory>

// return number of splits
unsigned int split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    unsigned int count = 0;
    while (std::getline(ss, item, delim)) {
        if(!item.empty()) {
            elems.push_back(item);
            count++;
        }
    }
    return count;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

template<typename ... Args>
std::string format(const std::string& format, Args ... args )
{
    size_t size = (size_t) (std::snprintf(nullptr, 0, format.c_str(), args ... ) + 1); // Extra space for '\0'
    std::unique_ptr<char[]> buf( new char[ size ] );
    std::snprintf( buf.get(), size, format.c_str(), args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside

}

#endif //ALLOCATORS_STRING_H
