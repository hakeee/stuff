//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_POINTER_H
#define ALLOCATORS_POINTER_H

struct pointer {
    static char* AlignTop(char* addr, size_t alignment) {
        assert(alignment % 2 == 0);
        if (alignment == 0)
            return addr;

        int remainder = (int) ((size_t) addr % alignment);
        if (remainder == 0)
            return addr;

        return addr + alignment - remainder;
    }
};


#endif //ALLOCATORS_POINTER_H
