//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_SINGLETHREAD_H
#define ALLOCATORS_SINGLETHREAD_H

class SingleThreadPolicy
{
public:
    inline void Enter(void) const {}
    inline void Leave(void) const {}
};

#endif //ALLOCATORS_SINGLETHREAD_H
