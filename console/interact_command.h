//
// Created by hakeee on 1/15/16.
//

#ifndef ALLOCATORS_INTERACT_COMMAND_H
#define ALLOCATORS_INTERACT_COMMAND_H

#include "../pch.h"
#include <functional>


struct interactCommand {

    using interactCommandFunction =
    std::function<void(interactCommand *, unsigned int argc, const char **args)>;


    interactCommand() {}

    interactCommand(const char *m_command, const char *m_argDesc, const char *m_desc, const char *m_help,
                    interactCommandFunction m_func)
            : m_command(m_command), m_argDesc(m_argDesc), m_desc(m_desc), m_help(m_help), m_func(m_func) { }

    const char *m_command;
    const char *m_argDesc;
    const char *m_desc;
    const char *m_help;

    // std::function<void(const interactCommand &command, const std::vector<std::string> &args)> m_func;
    interactCommandFunction m_func;
};

#endif //ALLOCATORS_INTERACT_COMMAND_H
