//
// Created by hakeee on 1/15/16.
//

#ifndef ALLOCATORS_INTERACT_H
#define ALLOCATORS_INTERACT_H

#include "../pch.h"

#include "interact_command.h"

class Console {
public:
  Console();

  ~Console();

  /**
   * Main 'interact' loop.
   *
   * @return true if the loop exitted normally, else false if an error occurred.
   */
  bool interact();

  void register_command(interactCommand *command);
  void unregister_command(const char * command);

private:

  bool m_stop;
  std::vector<interactCommand*> m_commands;


  std::vector<std::string> m_commandlog;

  const interactCommand * getIC(const char *command);

  /**
   * Print usage information for the specified command.
   *
   * @param command The command to print usage for.
   */

  static void describeCommand(std::string &dest, const interactCommand *command);

    interactCommand help, exit;
};


#endif //ALLOCATORS_INTERACT_H
