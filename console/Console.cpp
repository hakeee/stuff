//
// Created by hakeee on 1/15/16.
//

#include <string.h>
#include <functional>
#include "../util/string.h"
#include "Console.h"
#include <python2.7/Python.h>

Console::Console() :
    m_stop(false) {
  exit = interactCommand("exit",
                  0,
                  "Exit",
                  "Use exit to terminate program!",
                  [this](interactCommand *, unsigned int, const char **) {
                      m_stop = true;
                  }
  );
  register_command(&exit);
  help = interactCommand("help",
                 "[command]",
                 "Help: this text",
                 "speciel help text",
                 [this](interactCommand *, unsigned int argc, const char ** args) {
                     std::string descr;
                     if (argc == 2) {
                       auto gic = getIC(args[1]);
                       if(gic)
                         std::cout << gic->m_help << std::endl;
                       else
                         std::cout << "Unknown command, can't find help text!" << std::endl;
                     } else {
                       for (const auto &command: m_commands) {
                         describeCommand(descr, command);
                         std::cout << descr << std::endl;
                       }
                     }
                 });
  register_command(&help);
}

Console::~Console() {
}

bool Console::interact() {
  std::string line;
  std::vector<std::string> args;
  unsigned i;

  m_stop = false;
  while (!m_stop) {
    args.clear();
    std::cout << "> ";
    if (!getline(std::cin, line) || std::cin.eof())
      break;
    else if (std::cin.fail())
      return false;

    if (!split(line, ' ', args) || args.size() == 0)
      continue;

    for (i = 0; i < m_commands.size(); i++)
      if (strncasecmp(args[0].c_str(), m_commands[i]->m_command, args[0].length()) == 0)
        break;
    if (i < m_commands.size() && this->m_commands[i]->m_func) {
      std::vector<const char*> c_args;
      for (unsigned int j = 0; j < args.size(); ++j) {
        c_args.push_back(args[j].c_str());
      }
      this->m_commands[i]->m_func(this->m_commands[i], (unsigned int) args.size(), &c_args[0]);
    }
    else
      std::cout << "Unknown command '" << args[0] << "'" << std::endl;
  }
  return true;
}

void Console::describeCommand(std::string &dest, const interactCommand *command) {
  dest.clear();
  std::string cmdStr = command->m_command;
  if (command->m_argDesc != 0) {
    cmdStr += " ";
    cmdStr += command->m_argDesc;
  }
  dest = format("    %-30s%s", cmdStr.c_str(), command->m_desc);
}

void Console::register_command(interactCommand *command) {
  m_commands.push_back(command);
}

const interactCommand * Console::getIC(const char *command) {
  for(const auto& ic: m_commands)
    if(strcmp(command, ic->m_command) == 0)
      return ic;
  return nullptr;
}

void Console::unregister_command(const char *command) {

  for (unsigned int i = 0; i < m_commands.size(); ++i) {
    if(strcmp(command, m_commands[i]->m_command) == 0) {
      m_commands.erase(m_commands.begin() + i);
      break;
    }
  }
}
