//
// Created by hakeee on 12/23/15.
//

#include "compiler.h"
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <stdlib.h>

bool Compiler::Compile(const char* path, const char* sciptfolder, const char* libsfolder) {

    char l_in[256] = {0};
    char l_out[256] = {0};

    strcat(l_in, sciptfolder);
    strcat(l_in, path);

    strcat(l_out, libsfolder);
    strcat(l_out, path);
    strcat(l_out, ".so");

    pid_t pid = fork();
    int status = 1;
    if(pid == 0) {
        execl("/bin/c++"
                , "c++"
                , l_in
                , "-o"
                , l_out
                , "-shared"
                , "-fPIC"
                , "-std=c++17"
                , "-g3"
                , (char *) 0);
        exit(0);
    } else {
        waitpid(pid, &status, 0);
    }

    return status == 0;
}
