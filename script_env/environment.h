//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_ENVIRONMENT_H
#define ALLOCATORS_ENVIRONMENT_H

#include <mutex>
#include "../pch.h"
#include "scriptinterface.h"
#include "compiler.h"

class ScriptEnvironment {
public:
    class ScriptHolder {
    public:
        ScriptHolder(): script(nullptr), handle(nullptr), reloading(false), inited(false) { }
        ~ScriptHolder() {
            script = nullptr;
        }

        /// not threadsafe
        bool empty() const { return script == nullptr; }
        bool is_reloading() const { return this->reloading; }
        bool is_initialized() const { return this->inited; }
        void set_initialized(bool inited) { this->inited = inited; }
        ScriptHolder *get_this() const { return const_cast<ScriptHolder*>(this); }
        std::shared_ptr<IScript> get() const { return script; }

        /// lock the usage
        void lock() { mutex.lock(); }
        void unlock() { mutex.unlock(); }

    private:
        ScriptHolder(void *handle, IScript *script): script(script), handle(handle), reloading(false), inited(false)  { }

        void *get_handle() const { return handle; }

        void set_script(IScript* script) { this->script = std::shared_ptr<IScript>(script, [=](IScript*){
                //printf("Unloaded a scriptHolder\n");
            }); }
        void set_handle(void* handle) { this->handle = handle; }
        void set_reloading(bool reloading) { this->reloading = reloading; }

        std::shared_ptr<IScript> script;
        void    *handle;
        bool    reloading;
        bool    inited;
        std::mutex mutex;

        friend class ScriptEnvironment;
    };

    ~ScriptEnvironment();

    ScriptEnvironment::ScriptHolder * LoadScript(const char *filename);

    void unload(const char* filename);

    void setScriptsFolder(const char* path);

    void setLibsFolder(const char* path);

private:

    void internal_unload(const std::string& filename);

    const char* m_script_folder = nullptr;
    const char* m_libs_folder = nullptr;
    Compiler m_compiler;
    std::map<std::string, ScriptHolder> m_scripts;
};

#endif //ALLOCATORS_ENVIRONMENT_H
