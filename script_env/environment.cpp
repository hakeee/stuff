//
// Created by hakeee on 12/23/15.
//

#include "environment.h"
#include <string.h>
#include <dlfcn.h>

ScriptEnvironment::~ScriptEnvironment() {
    for(auto& item: m_scripts)
    {
        if(item.second.get_handle() != nullptr)
            unload(item.first.c_str());
    }
}

ScriptEnvironment::ScriptHolder * ScriptEnvironment::LoadScript(const char *filename) {
    if(m_libs_folder == nullptr || m_script_folder == nullptr)
        return nullptr;

//    auto begin = std::chrono::high_resolution_clock::now();

    bool compiled = m_compiler.Compile(filename, m_script_folder, m_libs_folder);
    if(!compiled)
        return nullptr;

//    auto end = std::chrono::high_resolution_clock::now();
//    std::cout << "Compile\t\t\t\t\t\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << "ns total" << std::endl;


    char l_libloadpath[256] = {0};
    strcat(l_libloadpath, m_libs_folder);
    strcat(l_libloadpath, filename);
    strcat(l_libloadpath, ".so\0");

    std::string sfilename(filename);

//    begin = std::chrono::high_resolution_clock::now();
    m_scripts[sfilename].lock();
    if(m_scripts[sfilename].get_handle() != nullptr) {
        m_scripts[sfilename].set_reloading(true);
        internal_unload(sfilename);
    }

//    end = std::chrono::high_resolution_clock::now();
//    std::cout << "strcat and loaded check\t\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << "ns total" << std::endl;
//
//    begin = std::chrono::high_resolution_clock::now();

//    void* handle = dlopen(l_libloadpath, RTLD_NOLOAD);
////    printf("%s\n", dlerror());
//    if(handle != nullptr) {
//        dlclose(handle);

        //internal_unload(filename);
//        printf("%s\n", dlerror());
//    }

//    handle = dlopen(l_libloadpath, RTLD_NOLOAD);
//    if(handle != nullptr)
//        dlclose(handle);

    void* handle = dlopen(l_libloadpath, RTLD_NOW | RTLD_LOCAL);
    if(handle == nullptr)
        return nullptr;

//    end = std::chrono::high_resolution_clock::now();
//    std::cout << "dlopen\t\t\t\t\t\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << "ns total" << std::endl;


//    begin = std::chrono::high_resolution_clock::now();
    void* getScript = dlsym(handle, "getScript");
    if(getScript == nullptr) {
        dlclose(handle);
        return nullptr;
    }
    IScript* script = reinterpret_cast<IScript*(*)()>(getScript)();
//    end = std::chrono::high_resolution_clock::now();
//    std::cout << "dlsym and reinterpret \t\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << "ns total" << std::endl;


//    begin = std::chrono::high_resolution_clock::now();
    m_scripts[sfilename].set_handle(handle);
    m_scripts[sfilename].set_script(script);
    m_scripts[sfilename].set_reloading(false);
    m_scripts[sfilename].unlock();
//    end = std::chrono::high_resolution_clock::now();
//    std::cout << "make_pair \t\t\t\t\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << "ns total" << std::endl;

    return m_scripts[sfilename].get_this();
}

void ScriptEnvironment::unload(const char* filename) {
    try {
        auto& pair = m_scripts.at(filename);

        pair.lock();
        pair.get()->exit();
        pair.set_script(nullptr);
        pair.set_initialized(false);
        if(dlclose(pair.get_handle()))
            printf("Couldn't unload %s\n", filename);
        pair.set_handle(nullptr);
        pair.unlock();
    } catch (std::exception) {
        //TODO: Log error
        std::cout << "unload scriptHolder failed: " << filename << std::endl;
    }
}

void ScriptEnvironment::setScriptsFolder(const char* path) {
    m_script_folder = path;
}

void ScriptEnvironment::setLibsFolder(const char* path) {
    m_libs_folder = path;
}

void ScriptEnvironment::internal_unload(const std::string &filename) {
    try {
        auto& pair = m_scripts.at(filename);

        pair.get()->exit();
        pair.set_script(nullptr);
        pair.set_initialized(false);
        if(dlclose(pair.get_handle()))
            printf("Couldn't unload %s\n", filename.c_str());
        pair.set_handle(nullptr);
    } catch (std::exception) {
        //TODO: Log error
        std::cout << "unload scriptHolder failed: " << filename << std::endl;
    }
}
