//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_COMPILER_H
#define ALLOCATORS_COMPILER_H

class Compiler {
    //TODO: should have some more compiler specific stuff here. Like compiler path and compiler type, debug flag
public:

    bool Compile(const char* path, const char* sciptfolder, const char* libsfolder);
};

#endif //ALLOCATORS_COMPILER_H
