//
// Created by hakeee on 1/11/16.
//

#include "../scriptinterface.h"
#include "../../pch.h"

class graphics_plugin_main : public IScript {
public:
    graphics_plugin_main() {
        logger.LogLine(VERBOSE_LEVEL::LOW, "load(), Compiled at ", __DATE__, " Time ", __TIME__);
    }

    ~graphics_plugin_main() {
        logger.LogLine(VERBOSE_LEVEL::LOW, "unload(), Compiled at ", __DATE__, " Time ", __TIME__);
    }

    bool initialize() {
        logger.LogLine(VERBOSE_LEVEL::LOW, "initialize(), Compiled at ", __DATE__, " Time ", __TIME__);
    }

    bool run() {
        logger.LogLine(VERBOSE_LEVEL::LOW, "run(), Compiled at ", __DATE__, " Time ", __TIME__);
    }

private:
    ConsoleLogger logger;
};


extern "C" {

graphics_plugin_main *getScript() {
    static graphics_plugin_main tC;
    return &tC;
}

}