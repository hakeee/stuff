//
// Created by hakeee on 12/23/15.
//

#include <iostream>
#include "../scriptinterface.h"
#include "../../pch.h"
#include "../../memory/policy/area/stack_area.h"

class test {
public:
    int32_t a;
    int8_t b;
    int8_t c;
};

class allocatortest : public IScript {
public:
    bool run() {
        ConsoleLogger logger;
        logger.LogLine(VERBOSE_LEVEL::LOW, "class allocatortest : public IScript!");

        char buffer[4096];
        StackArea stackarea(buffer, 4096);
        LinearCountMemArea simple_ma(stackarea);

        //T* t = (T*) allocator.allocate(sizeof(T), align, source_info(__FILE__, __LINE__, sizeof(T));
        test *a = (test *) simple_ma.allocate(sizeof(test), 0, source_info(__FILE__, __LINE__, 0));
        test *a2 = (test *) simple_ma.allocate(sizeof(test), 0, source_info(__FILE__, __LINE__, 0));
        test *a3 = (test *) simple_ma.allocate(sizeof(test), 0, source_info(__FILE__, __LINE__, 0));

        return true;
    }
};


extern "C" {

allocatortest *getScript() {
    static allocatortest tC;
    return &tC;
}

}