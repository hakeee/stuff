//
// Created by hakeee on 12/31/15.
//

#include "../../pch.h"
#include "../scriptinterface.h"
#include "../../directory/directory.h"
#include "../../directory/directory_watcher.h"

class directorytest: public IScript {

public:
    bool run() {
//        logger.LogLine(VERBOSE_LEVEL::NON, "directorytest!!");

        directory dir("/home/hakeee/ClionProjects/allocators/script_env/scripts/");
        //directory_watcher watcher(dir);
        //watcher.start_watch();
        return true;
    }

private:
    ConsoleLogger logger;
};


extern "C" {

directorytest *getScript() {
    static directorytest tC;
    return &tC;
}

}