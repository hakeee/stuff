//
// Created by hakeee on 2/8/16.
//

#include <thread>
#include "../scriptinterface.h"
#include "../../pch.h"
#include "../../directory/directory_watcher.h"

class graphics_plugin_main : public IGraphicsScript {
public:
    graphics_plugin_main()
    {
        logger.LogLine(VERBOSE_LEVEL::LOW, &*"Create graphics_plugin_main, Compiled at ", __DATE__, " Time ", __TIME__);
    }

    ~graphics_plugin_main() {
        logger.LogLine(VERBOSE_LEVEL::LOW, &*"Destroy graphics_plugin_main, Compiled at ", __DATE__, " Time ", __TIME__);

        graphicsServer->getRenderer().DeleteShader(shader);
        graphicsServer->getRenderer().DeleteTexture(texture);
        graphicsServer->getRenderer().DeleteBuffer(timeBuffer);
    }

    bool initialize(GraphicsServer::GraphicsDriver *graphicsServer, Kernel* kernel = nullptr) override {
        this->graphicsServer = graphicsServer;
        this->kernel = kernel;

        renderer = &graphicsServer->getRenderer();
        timelayout.data = &frame;
        timelayout.count = 1;
        timelayout.type = DataLayoutType::FLOAT;
        timeBuffer = renderer->CreateConstBuffer("time", {timelayout});

        v = kernel->getContentServer().getContent("/home/hakeee/ClionProjects/allocators/plugin/plugins/opengl/shaders/simpleShader.vert", false, true);
        f = kernel->getContentServer().getContent("/home/hakeee/ClionProjects/allocators/plugin/plugins/opengl/shaders/simpleShader.frag", false, true);
        pngimage = std::dynamic_pointer_cast<Image>(kernel->getContentServer().getContent("/home/hakeee/Untitled.png", true, true).lock());

        shader = renderer->CompileShader(v.lock().get(), f.lock().get());
        texture = renderer->CreateTexture(pngimage.lock(), "Test");
        if(shader == 0)
            return false;
        if(texture == 0)
            return false;
        renderer->UseShader(shader);
        return true;
    }

    bool run(GraphicsServer::GraphicsDriver *graphicsServer, Kernel* kernel = nullptr) override {
        frame += 0.256;
        if(!renderer->IsValidShader(shader))
            renderer->CompileShader(shader);
        if(!renderer->IsValidTexture(texture))
            renderer->ReloadTexture(texture);
        return true;
    }

    void render(GraphicsServer::GraphicsDriver *graphicsServer, Kernel* kernel = nullptr) override {
        renderer->Clear();
        if(shader == 0)
            return;

        renderer->UseShader(shader);
        //timelayout.data = &frame;
        //timeBuffer->SetData(shader, 0, timelayout);
        timeBuffer->Use(shader);
        renderer->UseTexture(texture, 0, "tex");
        renderer->DrawFullscreenQuad();
        renderer->UseShader(renderer->NoShader);
        //renderer->DrawString("Plugin here I come!!!", 0, 0, 1, 0, 0);
        //TODO: getMousePosition()
        renderer->DrawString("Can be changed at runtime in source", -0.8f, 0.1, 0, 0, 1);
        renderer->SwapBuffer();
        return;
    }

private:
    float frame = 0;
    ConsoleLogger       logger;
    Renderer::ShaderPtr shader;
    Renderer::TexturePtr texture;
    ConstBuffer         *timeBuffer;

    DataLayout          timelayout;

    GraphicsServer::GraphicsDriver *graphicsServer;
    Renderer            *renderer;
    Kernel              *kernel;

    std::weak_ptr<Content> v, f;
    std::weak_ptr<Image> pngimage;
};


extern "C" {

graphics_plugin_main *getScript() {
    static graphics_plugin_main tC;
    return &tC;
}

}