//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_SCRIPTINTERFACE_H
#define ALLOCATORS_SCRIPTINTERFACE_H

//#include "../plugin/servers/graphics.h"
//#include "../plugin/kernel.h"

#define register_script(class_name) extern "C" {\
app_script *getScript() {\
    static class_name tC;\
    return &tC;\
}}

class app;
extern "C" {

class IScript {
public:
    virtual bool initialize(app*) = 0;
    virtual bool run() = 0;
    virtual void exit() = 0;
};

//class IGraphicsScript: public IScript {
//public:
//    virtual bool initialize(GraphicsServer::GraphicsDriver *graphicsServer, Kernel* = nullptr) = 0;
//    virtual bool run(GraphicsServer::GraphicsDriver *graphicsServer, Kernel* = nullptr) = 0;
//    virtual void render(GraphicsServer::GraphicsDriver *graphicsServer, Kernel* = nullptr) = 0;
//
//private:
//    virtual bool initialize(app*) override final { return false; };
//    virtual bool run() override final { return false; };
//    virtual void exit() override {};
//};

};

#endif //ALLOCATORS_SCRIPTINTERFACE_H
