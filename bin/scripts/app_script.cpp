//
// Created by hakeee on 8/3/16.
//

#include "../../pch.h"
#include "../../script_env/scriptinterface.h"
#include "../../app/app.h"

class app_script : public IScript {
public:
    app_script() {
        //logger->LogLine(VERBOSE_LEVEL::LOW, "load(), Compiled at ", __DATE__, " Time ", __TIME__);
    }

    virtual ~app_script() {
        logger->LogLine(VERBOSE_LEVEL::LOW, "unload(), Compiled at ", __DATE__, " Time ", __TIME__);
    }

    bool initialize(app* app1) {
        this->app1 = app1;
        this->logger = &app1->getLogger();

        just_print = interactCommand{"as", 0, 0, 0, [this](interactCommand*, unsigned int , const char** ) {
            logger->LogLine(VERBOSE_LEVEL::DEBUG, "app_script", " Compiled at ", __DATE__, " Time ", __TIME__);
        }};
        app1->getConsole().register_command(&just_print);

        print_text = interactCommand{"text", 0, 0, 0, [this](interactCommand*, unsigned int argc, const char** args) {
            if(argc == 7) {
                this->app1->getW1()->draw_string(args[1],
                                           std::stof(args[2]),
                                           std::stof(args[3]),
                                           std::stof(args[4]),
                                           std::stof(args[5]),
                                           std::stof(args[6]));
            }
        }};
        app1->getConsole().register_command(&print_text);

        logger->LogLine(VERBOSE_LEVEL::LOW, "initialize(", app1, ")", " Compiled at ", __DATE__, " Time ", __TIME__);
        return true;
    }

    bool run() {
        app1->getW1()->clear(1,0,1,0);
        logger->LogLine(VERBOSE_LEVEL::LOW, "run(), Compiled at ", __DATE__, " Time ", __TIME__);
        return true;
    }

    void exit() override {
        app1->getConsole().unregister_command("as");
        app1->getConsole().unregister_command("text");
    }

private:
    app* app1;
    interactCommand just_print;
    interactCommand print_text;
    const ConsoleLogger *logger;
};

register_script(app_script)

