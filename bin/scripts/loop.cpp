//
// Created by hakeee on 8/3/16.
//

#include <cmath>
#include "../../pch.h"
#include "../../script_env/scriptinterface.h"
#include "../../app/app.h"
#include "../../graphics/opengl/shader.h"
#include "../../graphics/opengl/shader_components/frag.h"
#include "../../graphics/opengl/shader_components/vert.h"

class app_script : public IScript {
public:
    app_script() {
    }

    virtual ~app_script() {
    }

    bool initialize(app* app1) {
        this->app1 = app1;
        this->logger = &app1->getLogger();

        frequency = 2*3.1415f/180.0f;
        loop = 0;

        vert1 = new vert(this->app1, "shaders/test.vert");
        frag1 = new frag(this->app1, "shaders/test.frag");
        shader1 = new shader(this->app1);
        shader1->vertexShader = vert1;
        shader1->fragmentShader = frag1;
        shader1->compile();

        test_shader = std::make_shared<function_task>([this](){
            int a = 2;
        });

        return true;
    }

    bool run() {
        loop = ++loop % 1500;

        app1->getW1()->clear(
                sin(frequency*loop + 0) + 0.5,
                sin(frequency*loop + 2) + 0.5,
                sin(frequency*loop + 4) + 0.5,
                0);



        app1->getW1()->swap();
        return true;
    }

    void exit() override {
        delete shader1;
        delete vert1;
        delete frag1;
    }

private:
    app* app1;
    const ConsoleLogger *logger;

    std::shared_ptr<function_task> test_shader;

    shader*  shader1;
    frag*    frag1;
    vert*    vert1;

    float frequency;
    int loop;
};

register_script(app_script)

