#version 300 es

layout(location = 0) in highp vec3 in_position;

out vec2 frag_texcoords;

void main()
{
    gl_Position = vec4(in_position, 1.0);
    frag_texcoords = (in_position.xy + 1.0f) / 2.0f;
}
