//
// Created by hakeee on 2/21/16.
//

#ifndef ALLOCATORS_FRAG_H
#define ALLOCATORS_FRAG_H


#include <GL/glew.h>
#include <memory>
#include "../../../directory/reloadable_file.h"
#include "../task.h"
#include "../../../pch.h"

class app;
class frag : public reloadable_file {
public:
    frag(app* app1, const char* file_path);
    ~frag();

private:
    bool compile();

    GLuint                  shaderId = 0;

    bool                    loaded = false;

    app*                    app1 = nullptr;
    const ConsoleLogger&    logger;

    std::shared_ptr<function_task> task_compile;

    friend class shader;
};


#endif //ALLOCATORS_FRAG_H
