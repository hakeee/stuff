//
// Created by hakeee on 2/21/16.
//

#ifndef ALLOCATORS_VERT_H
#define ALLOCATORS_VERT_H


#include <GL/glew.h>
#include <memory>
#include "../../../directory/reloadable_file.h"
#include "../task.h"
#include "../../../pch.h"

class app;
class vert: public reloadable_file {
public:
    vert(app* app1, const char* file_path);
    ~vert();

private:
    bool compile();

    GLuint          shaderId = 0;

    bool            loaded = false;

    app*            app1 = nullptr;
    const ConsoleLogger&   logger;

    std::shared_ptr<function_task> task_compile;

    friend class shader;
};


#endif //ALLOCATORS_VERT_H
