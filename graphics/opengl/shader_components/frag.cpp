//
// Created by hakeee on 2/21/16.
//

#include <fstream>
#include "frag.h"
#include "../../../app/app.h"

frag::frag(app* app1, const char* file_path):
        reloadable_file(file_path),
        app1(app1),
        logger(app1->getLogger()) {
    callback = [this](const char* /*file_path*/, int /*mode*/) {
        this->app1->getW1()->queue_task(task_compile);
    };

    task_compile = std::make_shared<function_task>([this]() {
        compile();
    });
}

frag::~frag() {
    app1->getW1()->queue_task(std::make_shared<function_task>([=]() {
        if(shaderId != 0) {
            glDeleteShader(shaderId);
        }
    }));
    shaderId = 0;
}

bool frag::compile() {
    loaded = false;
    //TODO: Should probably be done before Compile task -- begin
    std::ifstream in;
    in.open(getFilePath(), std::ios_base::in);

    if (!in.is_open()) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "Couldn't open file at \"", getFilePath(), "\"");
        return false;
    }

    std::string shaderSource;
    shaderSource.assign(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());
    in.close();
    //TODO: Should probably be done before Compile task -- end


    shaderId = glCreateShader(GL_FRAGMENT_SHADER);
    if (shaderId == 0) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "GL Error: ", glGetError(), " in ", getFilePath());
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "glCreateShader failed!");
        return false;
    }

    const GLchar *shaderSourceChar = shaderSource.c_str();
    glShaderSource(shaderId, 1, &shaderSourceChar, 0);
    //printf("GL Error: %i\n",glGetError());
    glCompileShader(shaderId);

    //printf("GL Error: %i\n",glGetError());
    GLint shaderCompiled = GL_FALSE;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &shaderCompiled);

    //printf("GL Error: %i\n",glGetError());
    if (shaderCompiled != GL_TRUE) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "GL Error: ", glGetError(), " in ", getFilePath());
        GLint maxLength = 0;
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

        GLint logLength = 0;
        GLchar *log = new char[maxLength];
        glGetShaderInfoLog(shaderId, maxLength, &logLength, log);

        if (logLength >= 0)
            logger.LogLine(VERBOSE_LEVEL::DEBUG, log);

        delete[] log;
        glDeleteShader(shaderId);
        shaderId = 0;
        return false;
    }
    loaded = true;
    return true;
}
