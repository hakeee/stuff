//
// Created by hakeee on 8/1/16.
//

#ifndef ALLOCATORS_DRIVER_H
#define ALLOCATORS_DRIVER_H

#include <thread>
#include "window.h"
#include "window_setting.h"

class driver {
public:
    window* create_window(window_setting setting);
    void initialize();
    void stop();
protected:
private:

    std::vector<std::unique_ptr<window>> windows;
    std::thread             graphics_thread;
    Display                 *display = nullptr;
    XEvent                  event;
    SafeQueue<std::shared_ptr<task>> tasklist;
    bool running = false;

    friend class window;
};


#endif //ALLOCATORS_DRIVER_H
