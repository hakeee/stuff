//
// Created by hakeee on 2/21/16.
//

#ifndef ALLOCATORS_SHADER_H
#define ALLOCATORS_SHADER_H

#include <GL/glew.h>
#include <string>
#include <vector>
#include <memory>
#include "task.h"
#include "../../pch.h"

class vert;
class frag;
class app;
struct shader {

    shader(app* app1);
    ~shader();


    bool compile_impl();
    bool compile();

    bool is_valid();
    bool is_compiled();

    GLuint              program = 0;
    bool                loaded = false;

    vert*               vertexShader = nullptr;
    frag*               fragmentShader = nullptr;

    std::shared_ptr<function_task> task_compile;

    std::string         name;

    app*                app1 = nullptr;
    ConsoleLogger       logger;
};


#endif //ALLOCATORS_SHADER_H
