//
// Created by hakeee on 2/21/16.
//

#include "shader.h"
#include "../../logger/logger.h"
#include "shader_components/vert.h"
#include "shader_components/frag.h"
#include "../../app/app.h"

shader::shader(app* app1): app1(app1) {
    task_compile = std::make_shared<function_task>([this] {
        compile_impl();
    });
}

shader::~shader() {
    app1->getW1()->queue_task(std::make_shared<function_task>([=]() {
        if(program != 0) {
            glDeleteProgram(program);
        }
    }));
    program = 0;
}

bool shader::compile() {
    app1->getW1()->queue_task(task_compile);
    return true;
}

bool shader::compile_impl() {
    //TODO: add task
    loaded = false;
    if(program != 0)
        glDeleteProgram(program);

    program = glCreateProgram();
    if (program == 0) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "GL Error: ", glGetError(), " in ", name);
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "Couldn't create shader program!");
        return false;
    }

    if(!vertexShader->loaded && !vertexShader->compile()) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "Couldn't create shader program! vertexShader failed to compile!");

        return false;
    }
    if(!fragmentShader->loaded && !fragmentShader->compile()) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "Couldn't create shader program! fragmentShader failed to compile!");

        return false;
    }
    glAttachShader(program, vertexShader->shaderId);
    glAttachShader(program, fragmentShader->shaderId);
    glLinkProgram(program);
    //glUseProgram(program_);

    GLint linkSuccess = GL_TRUE;
    glGetProgramiv(program, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess != GL_TRUE) {
        logger.LogLine(VERBOSE_LEVEL::DEBUG, "GL Error: ", glGetError(), " in ", name);
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        GLint logLength = 0;
        GLchar *log = new char[maxLength];
        glGetShaderInfoLog(program, maxLength, &logLength, log);
        if (logLength > 0)
            logger.LogLine(VERBOSE_LEVEL::DEBUG, log);
        else
            logger.LogLine(VERBOSE_LEVEL::DEBUG,
                           "An error occured while linking shader program but no log was available!");

        delete[] log;

        if(program != 0) {
            glDeleteProgram(program);
            program = 0;
        }
        return false;
    }

    loaded = true;
    return true;
}

bool shader::is_valid() {
    return vertexShader->loaded && fragmentShader->loaded;
}

bool shader::is_compiled() {
    return this->loaded;
}

