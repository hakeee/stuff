//
// Created by hakeee on 8/1/16.
//

#include <X11/Xlib.h>
#include <stdlib.h>
#include "driver.h"

window* driver::create_window(window_setting setting) {
    auto window_ = std::make_unique<window>(this);
    auto raw_window_ = window_.get();

    tasklist.enqueue(std::make_shared<function_task>([this, window = window_.get(), setting](){
        window->initialize(setting, display);
    }));

    raw_window_->swap_task = std::shared_ptr<window::update_window_task>(
            new window::update_window_task(raw_window_, tasklist)
    );

    windows.emplace_back(std::move(window_));
    return raw_window_;
}

void driver::initialize() {
    running = true;
    graphics_thread = std::thread([this](){

        display = XOpenDisplay(NULL);
        const char * xserver = getenv("DISPLAY");

        if (display == 0)
        {
            printf("Could not establish a connection to X-server '%s'\n", xserver );
            exit(1);
        }

        while (running) {
            while(XPending(display) > 0) {
                XNextEvent(display, &event);
                switch (event.type) {
                    case Expose: {
                        //redraw = true;

                        glXSwapBuffers(event.xexpose.display, event.xexpose.window);
                        break;
                    }
                    case KeyPress: {
                        //keys[event.xkey.keycode] = true;
                        if(event.xkey.keycode == 53)
                            glXSwapBuffers(event.xkey.display, event.xkey.window);

                        break;
                    }
                    case KeyRelease: {
                        //keys[event.xkey.keycode] = false;
                        break;
                    }
//                    case MotionNotify:
//                        glXSwapBuffers(event.xmotion.display, event.xmotion.window);
                    case ConfigureNotify:
                        //XGetWindowAttributes(display, frame_window, &window_attributes);
                        //glViewport(0, 0, window_attributes.width, window_attributes.height);
                    default:
                        break;
                }
            }
            if(!tasklist.empty()) {
                auto task = tasklist.dequeue();
                task->run();
            }
        }
    });
}

void driver::stop() {
    running = false;
    graphics_thread.join();
}



