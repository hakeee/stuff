//
// Created by hakeee on 8/1/16.
//

#ifndef ALLOCATORS_WINDOW_H
#define ALLOCATORS_WINDOW_H

#include <atomic>
#include <GL/glew.h>
#include <X11/Xlib.h>
#include <GL/glx.h>
#include "../../thread/safe_queue.h"
#include "window_setting.h"
#include "task.h"

class driver;
class window {
public:
    window(driver *driver);

    ~window();

    bool isInitialized();

    void queue_task(std::shared_ptr<task> task);

    void clear(float r, float g, float b, float a);
    void swap();

    void draw_string(const char *str, const float x, const float y, const float r, const float b, const float g);
protected:
private:

    void initialize(window_setting setting, Display* display);

    std::atomic_bool initialized;
    driver *driver_ptr;

    XVisualInfo             *visual_info;
    Colormap                colormap;
    int                     depth;
    XSetWindowAttributes    frame_attributes;
    Window                  frame_window;
    XWindowAttributes       window_attributes;

    GLXContext              glx_context;

    // X11
    XFontStruct *fontinfo;
    GLuint base;
    unsigned int first, last;

    class update_window_task: public task {
    private:
        window* window_ptr;
        SafeQueue<std::shared_ptr<task>>& tasklist;
    public:

        update_window_task(window* ptr, SafeQueue<std::shared_ptr<task>> &tasklist)
                : window_ptr(ptr), tasklist(tasklist) { }

        void run();

        void enqueue();
    };

    std::shared_ptr<update_window_task> swap_task;

    friend class driver;

};


#endif //ALLOCATORS_WINDOW_H
