//
// Created by hakeee on 8/1/16.
//

#ifndef ALLOCATORS_WINDOW_SETTINGS_H
#define ALLOCATORS_WINDOW_SETTINGS_H

#include <string>

struct window_setting {
    std::string title;
    unsigned int height;
    unsigned int width;
};

#endif //ALLOCATORS_WINDOW_SETTINGS_H
