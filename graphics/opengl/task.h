//
// Created by hakeee on 8/1/16.
//

#ifndef ALLOCATORS_TASK_H
#define ALLOCATORS_TASK_H

#include <functional>

class task {
public:
    virtual void run() {};
};

class function_task: public task {
public:

    function_task(const std::function<void()> &func) : func(func) { }

    void run() override {
        func();
    }

    std::function<void()> func;
};

#endif //ALLOCATORS_TASK_H
