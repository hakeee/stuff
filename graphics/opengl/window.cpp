//
// Created by hakeee on 8/1/16.
//

#include <iostream>
#include <cstring>
#include <assert.h>
#include "window.h"
#include "driver.h"

window::window(driver *driver) : initialized(false) {
    this->driver_ptr = driver;
}

window::~window() {
    glDeleteLists(base, last - first + 1);
    XFreeFont(driver_ptr->display, fontinfo);
}

bool window::isInitialized() {
    return initialized;
}

void window::initialize(window_setting setting, Display* display) {
    (void)setting;
    depth  = DefaultDepth(display, 0);

    memset(&frame_attributes,0,sizeof(XSetWindowAttributes));

    frame_attributes.background_pixel = XWhitePixel(display, 0);


    int attributes[]{GLX_RGBA,
                     GLX_DEPTH_SIZE, 24,
                     GLX_DOUBLEBUFFER,
                     None};
    visual_info = glXChooseVisual(display, 0, attributes);

    if(visual_info == nullptr) {
        fprintf(stderr, "No appropiete visual found");
        assert(visual_info == nullptr);
    }

    colormap = XCreateColormap(display, RootWindow(display, 0), visual_info->visual, AllocNone);
    frame_attributes.colormap = colormap;

    frame_attributes.background_pixmap = None;
    frame_attributes.border_pixel      = 0;

    /* create the application window */
    frame_window = XCreateWindow(display, RootWindow(display, 0),
                                 0, 0, setting.width, setting.height, 0, depth,
                                 InputOutput, visual_info->visual, CWColormap | CWEventMask | CWBorderPixel | CWBackPixmap,
                                 &frame_attributes);

    XStoreName(display, frame_window, setting.title.c_str());


    glx_context = glXCreateContext(display, visual_info, NULL, GL_TRUE);
    glXMakeCurrent(display, frame_window, glx_context);

    GLenum glew = glewInit();
    if(glew != GLEW_OK)
        std::cerr << "Glew Error:" << glewGetErrorString(glew) << std::endl;

    XSelectInput(display, frame_window, ExposureMask | StructureNotifyMask | KeyPressMask | KeyReleaseMask);
    XGetWindowAttributes(display, frame_window, &window_attributes);

    XMapWindow(display, frame_window);

    Font id;
    fontinfo = XLoadQueryFont(driver_ptr->display,
//                              "-adobe-times-medium-r-normal--17-120-100-100-p-88-iso8859-1"
                              "10x20"
    );

    if (fontinfo == NULL) {
        printf("no font found\n");
        exit(0);
    }

    id = fontinfo->fid;
    first = fontinfo->min_char_or_byte2;
    last = fontinfo->max_char_or_byte2;

    base = glGenLists(last + 1);
    if (base == 0) {
        printf("out of display lists\n");
        exit(0);
    }
    glXUseXFont(id, first, last - first + 1, base + first);

    initialized = true;
}

void window::clear(float r, float g, float b, float a) {
    driver_ptr->tasklist.enqueue(std::make_shared<function_task>(function_task([r,g,b,a](){
        glClearColor(r,g,b,a);
        glClear(GL_COLOR_BUFFER_BIT);
    })));
}

void window::swap() {
    swap_task->enqueue();
}

void window::update_window_task::run() {
    glXSwapBuffers(window_ptr->driver_ptr->display, window_ptr->frame_window);
    //std::this_thread::sleep_for(std::chrono::milliseconds(16));//TODO: do something here, a repeating task with 16 ms sleep???? should probably be in the draw loop
    //tasklist.enqueue(std::shared_ptr<update_window_task>(new update_window_task(*this)));/* no diagnostic for this one */
}

void window::update_window_task::enqueue() {
    tasklist.enqueue(std::shared_ptr<window::update_window_task>(window_ptr->swap_task));
}

void window::draw_string(const char *str, const float x, const float y, const float r, const float b, const float g) {
    driver_ptr->tasklist.enqueue(std::make_shared<function_task>([this, r,g,b,x,y,str](){
        glColor3f(r,b,g);
        glRasterPos2f(x, y);
        glPushAttrib(GL_LIST_BIT);                                  // Pushes The Display List Bits
        glListBase(base);                                           // Sets The Base Character to 32
        glCallLists((GLsizei) strlen(str), GL_UNSIGNED_BYTE, str);  // Draws The Display List Text
        glPopAttrib();
    }));
}

void window::queue_task(std::shared_ptr<task> task) {
    driver_ptr->tasklist.enqueue(task);
}


