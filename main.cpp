

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

#include "app/app.h"

int main() {
    app app1;
    app1.initialize();
    app1.run();
    return 0;
}


#pragma clang diagnostic pop