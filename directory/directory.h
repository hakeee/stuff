//
// Created by hakeee on 12/31/15.
//

#ifndef ALLOCATORS_DIRECTORY_H
#define ALLOCATORS_DIRECTORY_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "../pch.h"

class directory {
public:
    directory(const char* path):
            m_cwd(path) {
        update();
    }

    void update() {
        DIR* dir;
        struct dirent* ent;
        struct stat buf;

        char namebuf[PATH_MAX];
        size_t base_len = strlen(m_cwd);

        if (!(dir = opendir(m_cwd))) {
            perror("failed to open directory");
            return;
        }

        while((ent = readdir(dir)) != NULL) {
            /* ignore .. and . */
            if (!strcmp(ent->d_name, "..") || !strcmp(ent->d_name, ".")) {
                continue;
            }

            snprintf(namebuf, sizeof(namebuf), "%s%s%s",
                     m_cwd,
                     (m_cwd[base_len - 1] == '/' ? "" : "/"),
                     ent->d_name);

            if (lstat(namebuf, &buf) != 0) {
                perror(namebuf);
                continue;
            }

            if (S_ISREG(buf.st_mode)) {
                m_files.push_back(namebuf);
            } else if (S_ISLNK(buf.st_mode)) {
                printf("LINK: %s\n", namebuf);
            } else if (S_ISDIR(buf.st_mode)) {
                m_directories.push_back(namebuf);
            }
        }

        closedir(dir);
    }

    const std::vector<std::string>& get_files() const {
        return m_files;
    }

    const std::vector<std::string>& get_directories() const {
        return m_directories;
    }

    std::vector<std::string>& get_files_no_const() {
        return m_files;
    }

    std::vector<std::string>& get_directories_no_const() {
        return m_directories;
    }

    const char* get_cwd() const {
        return m_cwd;
    }

private:
    const char* m_cwd;
    std::vector<std::string> m_files;
    std::vector<std::string> m_directories;
};

#endif //ALLOCATORS_DIRECTORY_H
