//
// Created by hakeee on 8/9/16.
//

#ifndef ALLOCATORS_RELOADABLE_FILE_H
#define ALLOCATORS_RELOADABLE_FILE_H

#include <string>
#include <functional>
#include "file.h"

class reloadable_file: public file {
public:
    reloadable_file(const char* file_path, std::function<void(const char*, int )> callback):
            file(file_path),
            callback(callback)
    {};

    reloadable_file(const char* file_path): file(file_path)
    {};

    virtual ~reloadable_file() {
    };

    void swap(std::function<void(const char* file_path, int mode)>& function) {
        callback.swap(function);
    }

    void force_reload() {
        callback(getFilePath(), -1);
    }

protected:
    std::function<void(const char* file_path, int mode)> callback;
};

#endif //ALLOCATORS_RELOADABLE_FILE_H
