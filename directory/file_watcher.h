//
// Created by hakeee on 2/22/16.
//

#ifndef ALLOCATORS_FILE_WATCHER_H
#define ALLOCATORS_FILE_WATCHER_H

#include <stdio.h>
#include <sys/inotify.h>
#include <iostream>
#include <functional>
#include <map>
#include <unistd.h>
#include "reloadable_file.h"

class file_watcher {
public:
    file_watcher() {
        initialize();
    };

    ~file_watcher() {
        for(auto wd : m_wd)
            (void) inotify_rm_watch(m_fd, wd.first);
        (void) close(m_fd);
    };

    void start_watch() {
        watching = true;
        ssize_t length = 0;
        int i = 0;
        fd_set set;
        struct timeval timeout;
        int rv;
        struct inotify_event *event;
        while (watching) {
            i = 0;

            FD_ZERO(&set); /* clear the set */
            FD_SET(m_fd, &set); /* add our file descriptor to the set */

            timeout.tv_sec = 0;
            timeout.tv_usec = 100000; // 0.1 seconds

            rv = select(m_fd + 1, &set, NULL, NULL, &timeout);

            //printf("select\n");
            if(rv == -1)
                perror("select"); /* an error accured */
            else if(rv == 0)
                continue;

            length = read(m_fd, m_eventbuffer, (size_t) BUF_LEN);

            if (length < 0) {
                perror("read");
                continue;
            }

            while ( i < length ) {

                event = ( struct inotify_event * ) &m_eventbuffer[i];

                std::function<void(const char *, int)> callback;
                m_wd[event->wd].swap(callback);

                if(callback)
                    callback(m_wd[event->wd].getFilePath(), event->mask);

                inotify_rm_watch(m_fd, event->wd);
                std::string file = m_wd[event->wd].getFilePath();
                m_wd.erase(event->wd);
                add(file.c_str(), std::move(callback));

                i += EVENT_SIZE + event->len;
            }
        }
        return;
    };

    void stop_watch() {
        watching = false;
    };

    bool add(const char* file, std::function<void(const char* file, int mask)> callback) {
        int new_wb = (inotify_add_watch(m_fd, file, (IN_CREATE | IN_DELETE | IN_MOVE | IN_MODIFY)));
        if (new_wb == -1)
        {
            printf("Couldn't add watch to \"%s\"\n", file);
            return false;
        }
        else
        {
            m_wd[new_wb] = reloadable_file(file, std::move(callback));
            //m_wd[new_wb] = std::pair(file, std::move(callback));
            //printf("Watching:: %s\n", file);
            return true;
        }
    };
    //TODO: remove(const char* file);

private:
    void initialize() {
        m_fd = inotify_init();
        if (m_fd < 0) {
            perror("inotify_init");
        }
    };

    static const int EVENT_SIZE = (sizeof(struct inotify_event));
    static const int BUF_LEN    = (1024 * (EVENT_SIZE + 16));

    int32_t m_fd;
    std::map<int, reloadable_file> m_wd;
    char m_eventbuffer[BUF_LEN];
    bool watching;
};

#endif //ALLOCATORS_FILE_WATCHER_H
