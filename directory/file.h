//
// Created by hakeee on 8/9/16.
//

#ifndef ALLOCATORS_FILE_H
#define ALLOCATORS_FILE_H

#include <string>

class file {
public:
    file(const char* file_path): file_path(file_path) {
    };

    virtual ~file() {
    };

    inline const char* getFilePath() const {
        return file_path.c_str();
    }

protected:
    std::string file_path;
};

#endif //ALLOCATORS_FILE_H
