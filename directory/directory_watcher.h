//
// Created by hakeee on 1/11/16.
//

#ifndef ALLOCATORS_DIRECTORY_WATCHER_H
#define ALLOCATORS_DIRECTORY_WATCHER_H

#include "../pch.h"
#include "directory.h"
#include <sys/inotify.h>

class directory_watcher {
public:
    directory_watcher(directory dir);
    directory_watcher(const char* path);

    ~directory_watcher();

    void start_watch();
    void stop_watch();

    std::function<void(const char* path, const char* file, int mask)> m_change_callback;
private:
    void initialize();

    static const int EVENT_SIZE = (sizeof(struct inotify_event));
    static const int BUF_LEN    = (1024 * (EVENT_SIZE + 16));

    directory m_directory;
    int32_t m_fd;
    int m_wd;
    char m_eventbuffer[BUF_LEN];
    bool watching;
};

directory_watcher::directory_watcher(directory dir)
        : m_directory(dir) {
    initialize();
}

directory_watcher::directory_watcher(const char* path)
        : m_directory(path) {
    initialize();
}

directory_watcher::~directory_watcher() {
    (void) inotify_rm_watch(m_fd, m_wd);
    (void) close(m_fd);
}

void directory_watcher::start_watch() {
    watching = true;
    ssize_t length = 0;
    int i = 0;
    fd_set set;
    struct timeval timeout;
    int rv;
    struct inotify_event *event;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    while (watching) {
        i = 0;

        FD_ZERO(&set); /* clear the set */
        FD_SET(m_fd, &set); /* add our file descriptor to the set */

        timeout.tv_sec = 0;
        timeout.tv_usec = 100000; // 0.1 seconds

        rv = select(m_fd + 1, &set, NULL, NULL, &timeout);

        //printf("select\n");
        if(rv == -1)
            perror("select"); /* an error accured */
        else if(rv == 0)
            continue;

        length = read(m_fd, m_eventbuffer, (size_t) BUF_LEN);

        if (length < 0) {
            perror("read");
            continue;
        }

        while ( i < length ) {

            event = ( struct inotify_event * ) &m_eventbuffer[i];

            std::vector<std::string>* vector_pointer = nullptr;
            //if (event->wd == m_wd) printf("%s", "In content: ");
            if (event->mask & IN_ISDIR) {
                vector_pointer = &m_directory.get_directories_no_const();
                //printf("Directory %s", event->name);
            } else {
                vector_pointer = &m_directory.get_files_no_const();
                //printf("File %s", event->name);
            }

            if (event->mask & IN_CREATE) {
                //printf(" was created.\n");
                vector_pointer->emplace_back(std::string(event->name));
            } else if (event->mask & IN_DELETE || event->mask & IN_DELETE_SELF) {

                //printf(" was deleted.\n");
                for(auto iterator = vector_pointer->begin(); iterator != vector_pointer->end(); iterator++)
                    if(iterator->compare(event->name)) {
                        vector_pointer->erase(iterator);
                        break;
                    }
            } else if (event->mask & IN_DELETE_SELF) {
                //printf(" was modifyed.\n");
            } else if (event->mask & IN_MOVED_TO) {
                //printf(" was moved.\n");
            }
            std::cout.flush();

            if(m_change_callback)
                m_change_callback(m_directory.get_cwd(), event->name, event->mask);

            i += EVENT_SIZE + event->len;
        }
    }
#pragma clang diagnostic pop
}

void directory_watcher::stop_watch() {
    watching = false;
}

void directory_watcher::initialize() {
    m_fd = inotify_init();
    if (m_fd < 0) {
        perror("inotify_init");
    }
    m_wd = inotify_add_watch(m_fd, m_directory.get_cwd(), (IN_CREATE | IN_DELETE | IN_MOVE | IN_DELETE_SELF));
    if (m_wd == -1)
    {
        printf("Couldn't add watch to %s\n", m_directory.get_cwd());
    }
    else
    {
        printf("Watching:: %s\n", m_directory.get_cwd());
    }
}

#endif //ALLOCATORS_DIRECTORY_WATCHER_H
