//
// Created by hakeee on 12/23/15.
//

#ifndef ALLOCATORS_CONSOLETYPE_H
#define ALLOCATORS_CONSOLETYPE_H

class ConsoleTypePolicy {
public:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "InfiniteRecursion"
    template<typename Arg, typename... Args>
    inline void Log(const Arg&& arg, const Args&&... args) const {
        std::cout << arg;
        Log(args...);
    }

    template<typename Arg, typename... Args>
    inline void Log(const Arg& arg, const Args&... args) const {
        std::cout << arg;
        Log(args...);
    }
#pragma clang diagnostic pop

    inline void Log() const {}
};

#endif //ALLOCATORS_CONSOLETYPE_H
