//
// Created by hakeee on 12/22/15.
//

#ifndef ALLOCATORS_LOGGER_H
#define ALLOCATORS_LOGGER_H

#include <bits/move.h>
#include <iostream>
#include "policy/console_type.h"

#include "../thread/policy/single_thread.h"

enum class VERBOSE_LEVEL: unsigned int {
    NON = 0,

    LOW,
    DEBUG,
    HIGH,

    FULL
};

template<unsigned int level>
class VerbosePolicy {
public:

    inline bool Accept(VERBOSE_LEVEL verbose_level) const {
        return static_cast<unsigned int>(verbose_level) <= level;
    }

    const VERBOSE_LEVEL verbosityLevel = static_cast<const VERBOSE_LEVEL>(level);

private:
};

template<class LoggerTypePolicy, class VerbosePolicy, class ThreadPolicy>
class Logger {
public:

    template<typename Arg, typename... Args>
    void Log(const VERBOSE_LEVEL verbose_level, const Arg& arg, const Args&... args) const {
        if(!m_verbosePolicy.Accept(verbose_level))
            return;

        m_threadPolicy.Enter();

        m_typePolicy.Log(arg, args...);

        m_threadPolicy.Leave();
    }

    template<typename Arg, typename... Args>
    inline void Log(const VERBOSE_LEVEL verbose_level, const Arg&& arg, const Args&&... args) const {
        if(!m_verbosePolicy.Accept(verbose_level))
            return;

        m_threadPolicy.Enter();

        m_typePolicy.Log(std::forward<Arg>(arg), std::forward<Args>(args)...);

        m_threadPolicy.Leave();
    }

    template<typename... Args>
    inline void LogLine(const VERBOSE_LEVEL verbose_level, const Args&... args) const {
        Log(verbose_level, args..., '\n');
    }

    template<typename... Args>
    inline void LogLine(const VERBOSE_LEVEL verbose_level, const Args&&... args) const {
        Log(verbose_level, std::forward<Args>(args)..., '\n');
    }

private:

    LoggerTypePolicy m_typePolicy;
    VerbosePolicy m_verbosePolicy;
    ThreadPolicy m_threadPolicy;
};

#endif //ALLOCATORS_LOGGER_H
