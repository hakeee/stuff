//
// Created by hakeee on 12/21/15.
//

#ifndef ALLOCATORS_PCH_H
#define ALLOCATORS_PCH_H

#include <memory>
#include <unordered_map>
#include <map>
#include <vector>

#include "memory/policy/allocators/linear_allocator.h"
#include "memory/policy/bounds_check/no_bounds_checking.h"
#include "memory/policy/tagging/no_memory_tagging.h"
#include "memory/policy/tracking/count_memory_tracking.h"
#include "memory/policy/tracking/file_memory_tracking.h"
#include "memory/memory_area.h"
#include "memory/source_info.h"
#include "thread/policy/single_thread.h"


typedef Logger<ConsoleTypePolicy, VerbosePolicy<static_cast<unsigned int>(VERBOSE_LEVEL::DEBUG)>, SingleThreadPolicy> ConsoleLogger;
typedef memory_area<LinearAllocator<ConsoleLogger>, SingleThreadPolicy, NoBoundsChecking, FileMemoryTracking<ConsoleLogger>, NoMemoryTagging> LinearCountMemArea;


#endif //ALLOCATORS_PCH_H
