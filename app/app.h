//
// Created by hakeee on 8/2/16.
//

#ifndef ALLOCATORS_APP_H
#define ALLOCATORS_APP_H

#include "../pch.h"
#include "../console/Console.h"
#include "../graphics/opengl/driver.h"
#include "../script_env/environment.h"

class app {
public:

    void initialize();
    void run();

    const ConsoleLogger& getLogger() const {
        return logger;
    }

    const driver &getGdriver() const {
        return gdriver;
    }

    Console &getConsole() {
        return console;
    }

    window *getW1() const {
        return w1;
    }

private:

    ConsoleLogger logger;
    driver gdriver;
    Console console;
    window *w1;
    ScriptEnvironment scriptEnvironment;
    ScriptEnvironment::ScriptHolder * app_script, *loop_script;

    std::thread loop_thread;

    interactCommand w1_swap;
};


#endif //ALLOCATORS_APP_H
