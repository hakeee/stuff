//
// Created by hakeee on 8/2/16.
//

#include "app.h"

void app::initialize() {
    logger.LogLine(VERBOSE_LEVEL::DEBUG, "App was initialized!");

    // create and init dispatcher
    // create and init scheduler

    gdriver.initialize();
    w1 = gdriver.create_window(window_setting{.title = "First Window", .height = 100, .width = 100});

    scriptEnvironment.setScriptsFolder("./scripts/");
    scriptEnvironment.setLibsFolder("./scripts/libs/");
    app_script = scriptEnvironment.LoadScript("app_script.cpp");
    app_script->lock();
    app_script->get()->initialize(this);
    app_script->unlock();

    loop_script = scriptEnvironment.LoadScript("loop.cpp");
    loop_script->lock();
    loop_script->get()->initialize(this);
    loop_script->unlock();


    // register commands

    w1_swap = interactCommand ("swap", 0, 0, 0, [this](auto, unsigned int , const char** ) {
        w1->swap();
    });
    console.register_command(&w1_swap);

    interactCommand* glclear = new interactCommand("glclear", 0, 0, 0, [this](auto, unsigned int argc, const char** args)->void {
        if(argc != 5)
            return;


        w1->clear((int8_t) std::stoi(args[1]),
                  (int8_t) std::stoi(args[2]),
                  (int8_t) std::stoi(args[3]),
                  (int8_t) std::stoi(args[4]));
    });
    console.register_command(glclear);

    interactCommand* run = new interactCommand("run", 0, 0, 0, [this](auto, auto, auto) {
        app_script->lock();
        if(!app_script->empty()) {
            app_script->get()->run();
        }
        app_script->unlock();
    });
    console.register_command(run);
    interactCommand* un = new interactCommand("un", 0, 0, 0, [this](auto, auto, auto) {
        console.unregister_command("as");
    });
    console.register_command(un);
    interactCommand* reload = new interactCommand("reload", 0, 0, 0, [this](auto, auto, auto) {
        scriptEnvironment.LoadScript("app_script.cpp");

        app_script->lock();
        app_script->get()->initialize(this);
        app_script->unlock();
    });
    console.register_command(reload);

}


void app::run() {
    logger.LogLine(VERBOSE_LEVEL::DEBUG, "App was started!");

    bool running = true;

    loop_thread = std::thread([this, &running](){
        while(running) {
            loop_script->lock();
            loop_script->get()->run();
            loop_script->unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(16));
        }
    });

    // start stuff before this
    console.interact();
    // stop stuff after this
    running = false;
    loop_thread.join();
    gdriver.stop();

    logger.LogLine(VERBOSE_LEVEL::DEBUG, "App has terminated!");
}
